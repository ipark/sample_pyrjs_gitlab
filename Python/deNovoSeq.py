import re, sys, difflib, os, math, posix
from itertools import groupby, product
from collections import defaultdict, OrderedDict
from textwrap import fill
from random import sample
import string


AA_1to3 = {'G':'GLY', 'A':'ALA', 'V':'VAL', 'L':'LEU', #'I':'ILE', 
           'M':'MET', 'F':'PHE', 'W':'TRP', 'P':'PRO', 'S':'SER', 
           'T':'THR', 'C':'CYS', 'Y':'TYR', 'Q':'GLN', #'K':'LYS',
           'N':'ASN', 'D':'ASP', 'E':'GLU', 'R':'ARG', 'H':'HIS'} 


def denovo_seq(denovo_fas_file):
    seqIDk_denovoFPv_dic = {}
    denovo_fas = open(denovo_fas_file, 'r').readlines()
    for i, x in enumerate(denovo_fas):
        x = x.strip()
        if re.search("^>", x):
            k = x.replace(">", "")
            v = denovo_fas[i+1]
            seqIDk_denovoFPv_dic[k] = v
    return seqIDk_denovoFPv_dic


def blast_coverage(denovo_fas_file, blast_result, DB):
    seqIDk_denovoFPv_dic = denovo_seq(denovo_fas_file)
    chainID = (blast_result.replace(".out", "").split('_'))[-1]
    #print chainID 

    #print blast_result + '========================'
    PDBk_SEQv_dic = defaultdict(list)
    #SEQk_PDBv_dic = defaultdict(list)

    # pdbaa DB
    ###########
    #qseqid  sseqid                  pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq    sseq
    # 0      1                       2      3      4        5       6      7    8      9    10     11        12      13 
    #seq1    gi|171848866|pdb|2QL1|A 25.44  114    68       4       520    629  52     152  0.91   33.1    QFNWYVDG VEVHGQCCCCMA---
    #        0  1         2   3    4
    #------------------
    # KABAT1999 DB
    ##############
    #qseqid  sseqid                  pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq    sseq
    # 0      1                       2      3      4        5       6      7    8      9    10     11        12      13 
    #seq4    gi|27168|ref|Hc_27168|  70.00   20    6        0       194    213  309    328 0.003   34.7    MHEALHNHYTQKKRKKKKKK    MHEALHNHYTQKSTSKSAGK
    #        0  1       2   3      
    #------------------
    # NCBIgermline DB
    ##############
    #qseqid         sseqid                      pident length mismatch gapopen qstart qend sstart   send evalue bitscore qseq    sseq
    # 0              1                           2      3      4        5       6      7    8       9    10     11        12      13 
    #seq1254	gnl|blast|mVH1_J558.62pg.158	51.61	31      15      0       11      41	7	    99	0.002	30.4	QSQQPGAELVQPMGSSPESSQFLVALFFSTG	QLQQPGAELVKPGASVKLSARLLATLSPATG				
    #seq1456	gnl|blast|mVH1_J558.35	        78.95	19	    4	    0	    4	    22	238	    294	0.002	30.4	YMQNSSLTSQDSAVYYFQY	YMQLSSLTSEDSAVYYCTY
    #seq7674	gnl|blast|hVH_IGHV3-66*01	    55.56	27	    12	    0	    1	    27	7	    87	3e-04	33.1	QEEQSGAELVQPGASVQFSCQASGYQV	QLVESGGGLVQPGGSLRLSCAASGFTV
    #            0    1       2
    
    with open(blast_result, 'r') as h:
        for _ in h:
            seqID_info = []
            it = _.split()        
            seqID = it[0]
            #################################
            if DB == "KABAT1999":
                pdbID = it[1].split('|')[1]
            elif DB == "NCBIgermline_pr":
                pdbID = it[1].split('|')[2]
            elif DB == "pdbaa":
                pdbID = it[1].split('|')[3]
            #################################
            Pstart = it[8]; Pend = it[9]
            Dseq = it[12]; Pseq = it[13]
            seqID_info = [seqID, Pstart, Pend, Dseq, Pseq] # 0seqID, 1Pstart, 2Pend, 3Dseq, 4Pseq
            PDBk_SEQv_dic[pdbID].append(seqID_info)
    reorder = []
    pdbs = PDBk_SEQv_dic.keys()
    pdbs.sort(key=lambda line:len(PDBk_SEQv_dic[line]))
    for pdb in pdbs:
        res_coverage = []                                          
        for v in PDBk_SEQv_dic[pdb]:
            #seqID_info = [seqID, Pstart, Pend, Dseq, Pseq] # 0seqID, 1Pstart, 2Pend, 3Dseq, 4Pseq, 5bitscore
            s = int(v[1])
            e = int(v[2])
            for i in range(s, e+1):
                if not i in res_coverage:
                    res_coverage.append(i)
        string = 'DB_%s %s_%s covered total %d denovo Seqs over %d residues at %s' % (DB, pdb, chainID, len(PDBk_SEQv_dic[pdb]), len(res_coverage), list(ranges(res_coverage)))
        #          0     1    2       3     4     5   6     7    8   9      10  11  
        reorder.append(string)
        reorder.sort(key=lambda line:int((line.split()[4]))) # sorted number of denovo seq
        #reorder.sort(key=lambda line:int( (line.split()[11]).split(',')[0].replace("[(", "") )) # sorted by variable region residue number (1-125)
    with open(blast_result.replace(".out", ".coverage"), 'w') as f:
        print >> f, '\n'.join(reorder)
    
    """
    DB_KABAT1999 100_H coverted ...
        0        1    
    DB_pdbaa 1HZH_H covered total 11 denovo seqs over 220 residues at [(104, 178), (232, 349), (399, 425)]
        0     1   
    """
    template_list = []
    for m in reorder[-10:]:
        print m
        template_list.append(m.split()[1])
    print template_list
    return template_list
        
        
def ranges(mylist):
    mylist.sort()
    for k, v in groupby(enumerate(mylist), lambda (x, y): y - x):
        v = list(v)
        yield v[0][1], v[-1][1]


def BlastTemplate_ContigAlign(blast_result, DB, chainID):

    PDBk_DseqPseqv_dic = defaultdict(list)

    """
    PDBk_DseqPseqv_dic = { 
            pdbID1: [ [DenovoSeq1, PDBSeq1], [DenovoSeq2, PDBSeq2], ..., ], 
            pdbID2: [ [DenovoSeq1, PDBSeq1], [DenovoSeq2, PDBSeq2], ..., ], 
                           ... 
            pdbID#: [ [DenovoSeq1, PDBSeq1], [DenovoSeq2, PDBSeq2], ..., ], 
    }
    """


    # pdbaa DB
    ###########
    #qseqid  sseqid                  pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq    sseq
    # 0      1                       2      3      4        5       6      7    8      9    10     11        12      13 
    #seq1    gi|171848866|pdb|2QL1|A 25.44  114    68       4       520    629  52     152  0.91   33.1    QFNWYVDG VEVHGQCCCCMA---
    #        0  1         2   3    4
    #------------------
    # KABAT1999 DB
    ##############
    #qseqid  sseqid                  pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq    sseq
    # 0      1                       2      3      4        5       6      7    8      9    10     11        12      13 
    #seq4    gi|27168|ref|Hc_27168|  70.00   20    6        0       194    213  309    328 0.003   34.7    MHEALHNHYTQKKRKKKKKK    MHEALHNHYTQKSTSKSAGK
    #        0  1       2   3      
    
    with open(blast_result, 'r') as h:
        for _ in h:
            seqID_info = []
            it = _.split()        
            seqID = it[0]
            Pstart = int(it[8]) #sstart
            Dseq = it[12] #qseq
            Pseq = it[13] #sseq
            #################################
            if DB == "KABAT1999":
                pdbID = it[1].split('|')[1]
            else:
                pdbID = it[1].split('|')[3]
            #################################
            seqID_info = [Dseq, Pseq, Pstart, seqID] # 0Dseq, 1Pseq, 2Pstart, 3seqID
            PDBk_DseqPseqv_dic[pdbID].append(seqID_info)
    reorder = []
    pdbs = PDBk_DseqPseqv_dic.keys()
    pdbs.sort(key=lambda line:len(PDBk_DseqPseqv_dic[line]))


    for pdb in pdbs[-10:]:
        #################################                                                                                 
        PDB_ID = pdb + '_' + chainID
        if DB == "KABAT1999":
            cmd = "blastdbcmd -db ~/Progs/blastplus/db/%s -dbtype prot -entry '%s' -outfmt %%f -out /tmp/ipark/%s.fas" \
% (DB, pdb, PDB_ID)
        elif DB == "NCBIgermline_pr":
            cmd = "blastdbcmd -db ~/Progs/blastplus/db/%s -dbtype prot -entry '%s' -outfmt %%f -out /tmp/ipark/%s.fas" \
% (DB, pdb, pdb)
        elif DB == "pdbaa":
            cmd = "blastdbcmd -db ~/Progs/blastplus/db/%s -dbtype prot -entry '%s' -outfmt %%f -out /tmp/ipark/%s.fas" \
% (DB, PDB_ID.replace("_",":"), PDB_ID)
        #################################
        print cmd
        os.system(cmd)                                                                          
        seq1R_str = [] 
        align_file = "/tmp/ipark/%s.ali" % PDB_ID
        with open(align_file, 'w') as z:

            with open("/tmp/ipark/%s.fas" % PDB_ID, 'r') as f:                                             
                    for _ in f:
                        _ = _.strip()
                        if re.search("^>", _):
                            if re.search(" >", _):
                                _ = ">" + _.split(">")[1]
                            seq1R_str.append(_+'\n')
                        else:
                            seq1R_str.append(_)
                    fullSeq = ''.join(seq1R_str)
                    #print fullSeq, len(fullSeq)
                    print >> z, fullSeq
                                                                                                        
            ########## denovo_seq 
            for dstring, pstring, loc, dseqid in [[a,b,c,d] for a,b,c,d in PDBk_DseqPseqv_dic[pdb]]:
                                                                                                          
                for match in re.finditer(pstring, fullSeq):
                    print >> z, '>denovo%s' % (dseqid)
                    #prefix = '-' * int(loc)
                    #print "%s%s" % (prefix, pstring)
                    #print >> z, "%s%s" % (prefix, dstring)
                    print >> z, dstring
        cmd = "/home/ipark/Progs/clustalO -i %s --outfmt=clu --resno  > %s.clu" % (align_file, PDB_ID)
        os.system(cmd)

    denovo_fas_file = "20K.fas"; blast_result = '20K_KABAT1999_blastp_myformat_H.out'; DB = "KABAT1999"


def get_TemplateSeq_CDRFRstring_from_accession(PDB_chain, chainID):

    PDB_ID = PDB_chain.split('_')[0]
    if PDB_ID.isdigit():  # KABAT numbering #####_VH
        cmd = "blastdbcmd -db ~/Progs/blastplus/db/KABAT1999 -dbtype prot -entry '%s' -outfmt %%f -out /tmp/ipark/%s.fas" % \
        (PDB_ID, PDB_chain)
    else: # pdbaa numbering 1XBX_VH
        cmd = "blastdbcmd -db ~/Progs/blastplus/db/pdbaa -dbtype prot -entry '%s' -outfmt %%f -out /tmp/ipark/%s.fas" % \
        (PDB_ID + ":" + chainID, PDB_chain)
    #print cmd
    posix.system(cmd)
    mytemplate = ''.join([line.strip() for line in open("/tmp/ipark/%s.fas" % PDB_chain, 'r').readlines() if not re.search("^>", line)])
    ####
    CDR_FR_string = KABATnumbering4template(mytemplate, PDB_chain, chainID)
    return mytemplate, CDR_FR_string

def get_TemplateSeq_CDRFRstring_from_accession_NCBIgermline(PDB_chain, chainID):

    PDB_ID = PDB_chain.split('_')[0] + '_' + PDB_chain.split('_')[1]
    cmd = "blastdbcmd -db ~/Progs/blastplus/db/NCBIgermline_pr -dbtype prot -entry '%s' -outfmt %%f -out /tmp/ipark/%s.fas" % (PDB_ID, PDB_ID.replace("/", "-"))
    posix.system(cmd)
    mytemplate = ''.join([line.strip() for line in open("/tmp/ipark/%s.fas" % PDB_ID.replace("/", "-"), 'r').readlines() if not re.search("^>", line)])
    mytemplate = mytemplate.split('*')[0]
    ####
    PDB_ID = PDB_ID.replace("/", "-")
    CDR_FR_string = KABATnumbering4template(mytemplate, PDB_ID, chainID)
    return mytemplate, CDR_FR_string



def AligntContigs_onto_MyTemplate(contigs_set, mytemplate, denovoProgs, CDR_FR_string, PDB_chain, info):

    match_contigs = {}
    for ct in contigs_set: 
        for match in re.finditer(ct.replace("L", "[L|I]"), mytemplate.upper()):
            match_contigs[int(match.start())] = ct
        for match in re.finditer(ct.replace("I", "[I|L]"), mytemplate.upper()):
            match_contigs[int(match.start())] = ct
        for match in re.finditer(ct.replace("Q", "[Q|K]"), mytemplate.upper()):
            match_contigs[int(match.start())] = ct
        for match in re.finditer(ct.replace("K", "[K|Q]"), mytemplate.upper()):
            match_contigs[int(match.start())] = ct
    
    """
    prev_k = 0
    prev_v = ''
    """
    hold_lines = []
    contigs_all = []
    for k,v in sorted(match_contigs.iteritems(), key=lambda (k,v):(int(k),v)):  
        prefix_v = '.' * k + v
        contigs_all.append(prefix_v)
    contigs_all = OrderedDict.fromkeys(contigs_all).keys()

    CDR_FR_lines = []
    # print full sequence per 100-res unit
    if len(mytemplate) >= 100:
        for x in xrange(0, len(mytemplate), 100):                                                                 
            # string[0 : x] => string[(x-1)*100 : x]                                                            
            ########################################
            #print >> z, mytemplate[(x-100):x] + " " + str(x)                                      
            if x > 0:                                                                                              
                hold_lines.append(mytemplate[(x-100):x] + " " + str(x))                                     
                if len(mytemplate[x:]) > len(CDR_FR_string[x:]):
                    CDR_FR_lines.append(CDR_FR_string[(x-100):x] + "-"*(100-len(CDR_FR_string[(x-100):x])))
                else:
                    CDR_FR_lines.append(CDR_FR_string[(x-100):x])
                for _ in contigs_all: 
                    if (len(_) <= x + 5) and (len(_) >= x - 100):
                        #if re.search("^%s" % ('.'*x), _):
                        #print >> z, _[(x-100):]
                        hold_lines.append( _[(x-100):])
                ####################################################
                # last line of seq with < 100 residues
                char_left = len(mytemplate[x:])
                if char_left < 100:
                    #print >> z, mytemplate[x:] + " "*(100 - char_left + 1) +  str(len(mytemplate))
                    hold_lines.append(mytemplate[x:] + " "*(100 - char_left + 1) +  str(len(mytemplate)))
                    if len(mytemplate[x:]) > len(CDR_FR_string[x:]):
                        CDR_FR_lines.append(CDR_FR_string[x:] + "-"*(len(mytemplate[x:])- len(CDR_FR_string[x:])))
                    else:
                        CDR_FR_lines.append(CDR_FR_string[x:])
                    for _ in contigs_all:                          
                        if len(_) >= x:
                            #print >> z, _[x:]
                            hold_lines.append(_[x:])
    else: # len(mytemplate) < 100
        # first line is last line with seq length < 100 residues
        for x in xrange(0, len(mytemplate), 100):
            hold_lines.append(mytemplate[x:] + " "*(100 - len(mytemplate[x:]) + 1) +  str(len(mytemplate))) 
            if len(mytemplate[x:]) > len(CDR_FR_string[x:]):
                CDR_FR_lines.append(CDR_FR_string[x:] + "-"*(len(mytemplate[x:])- len(CDR_FR_string[x:])))
            else:
                CDR_FR_lines.append(CDR_FR_string[x:])
            for _ in contigs_all:                          
                if len(_) >= x:
                    hold_lines.append(_[x:])
            
    

    # final coverage checking
    prev_i = 0
    with open("%s_%s_StringentMatch.ali" % (denovoProgs, PDB_chain), 'w') as z:
        covered_index = []; flag = 0
        total_X = 0
        template_line_count = 0
        for i,_ in enumerate(hold_lines):
            _ = _.strip()
            if flag == 1: # found template line with last residue number
                for loc,cha in enumerate(_):
                    if cha != ".":
                        covered_index.append(int(loc))
                if i < len(hold_lines)-1:
                    if re.search("[0-9]", hold_lines[i+1]):                      
                        string = []
                        for temp_i, temp_x in enumerate(template_.split()[0]):
                            if not temp_i in covered_index:
                                #string.append(temp_x.lower())
                                string.append('X')
                                total_X += 1
                            else:
                                string.append('o')
                        if len(CDR_FR_lines[template_line_count]) > 0:
                            print >> z, CDR_FR_lines[template_line_count] + " FR/CDR"
                        else:
                            print >> z, '-'*len(template_) + " FR/CDR"
                        ###print >> zz, CDR_FR_lines[template_line_count]
                        template_line_count += 1
                        print >> z, ''.join(string)  + " coverage"
                        ###print >> zz, ''.join(string)  + " coverage"
                        #####################
                        noshort_lines = []
                        for hl in hold_lines[prev_i:i]:
                            if len(hl) > 2:
                                noshort_lines.append(hl)    
                        noshort_set = OrderedDict.fromkeys(noshort_lines).keys()
                        #print >> z, '\n'.join(hold_lines[prev_i:i])
                        print >> z, '\n'.join(noshort_set)
                        ###print >> zz, hold_lines[prev_i]
                        #####################
                        print >> z
                        print >> z
                        ###print >> zz
                        ###print >> zz
                        covered_index = []; flag = 0
                elif i == len(hold_lines) - 1:
                    string = []
                    for temp_i, temp_x in enumerate(template_.split()[0]):
                        if not temp_i in covered_index:
                            #string.append(temp_x.lower())
                            string.append('X')
                            total_X += 1
                        else:
                            string.append('o')
                    #print ''.join(string) + " " + str(template_.split()[1])
                    if len(CDR_FR_lines[template_line_count]) > 0:
                        print >> z, CDR_FR_lines[template_line_count] + " " * (100-len(template_.split()[0])+1) + "FR/CDR"
                    else:
                        print >> z, '-'*len(template_) + " " * (100-len(template_.split()[0])+1) + "FR/CDR"
                    ###print >> zz, CDR_FR_lines[template_line_count]
                    print >> z, ''.join(string)  + " " * (100-len(template_.split()[0])+1) + "coverage"
                    ###print >> zz, ''.join(string)  + " " * (100-len(template_.split()[0])+1) + "coverage"
                    #####################
                    noshort_lines = []
                    for hl in hold_lines[prev_i:i]:
                        if len(hl) > 2:
                            noshort_lines.append(hl)    
                    noshort_set = OrderedDict.fromkeys(noshort_lines).keys()
                    #####################
                    #print >> z, '\n'.join(hold_lines[prev_i:i])
                    print >> z, '\n'.join(noshort_set)
                    ###print >> zz, hold_lines[prev_i]
                    print >> z
                    print >> z
                    ###print >> zz
                    ###print >> zz
                    
            else: # not yet found template line with last residue number
                if re.search("[0-9]", _):
                    template_ = _
                    prev_i = i
                    flag = 1
                    
        print >> z, info

    ###zz.close()
        
        
def get_TemplateSeq_CDRFRstring_from_fasfile(fas_file, chainID):

    mytemplate = ''.join([line.strip() for line in open(fas_file, 'r').readlines() if not re.search("^>", line)])
    PDB_chain = os.path.basename(fas_file)
    CDR_FR_string = KABATnumbering4template(mytemplate, PDB_chain, chainID)
    return mytemplate, CDR_FR_string

def AligntContigs_onto_MyTemplate(contigs_set, mytemplate, denovoProgs, CDR_FR_string, PDB_chain, info):

    match_contigs = {}
    for ct in contigs_set: 
        for match in re.finditer(ct.replace("L", "[L|I]"), mytemplate.upper()):
            match_contigs[int(match.start())] = ct
        for match in re.finditer(ct.replace("I", "[I|L]"), mytemplate.upper()):
            match_contigs[int(match.start())] = ct
        for match in re.finditer(ct.replace("Q", "[Q|K]"), mytemplate.upper()):
            match_contigs[int(match.start())] = ct
        for match in re.finditer(ct.replace("K", "[K|Q]"), mytemplate.upper()):
            match_contigs[int(match.start())] = ct
    
    """
    prev_k = 0
    prev_v = ''
    """
    hold_lines = []
    contigs_all = []
    for k,v in sorted(match_contigs.iteritems(), key=lambda (k,v):(int(k),v)):  
        prefix_v = '.' * k + v
        contigs_all.append(prefix_v)
    contigs_all = OrderedDict.fromkeys(contigs_all).keys()

    CDR_FR_lines = []
    # print full sequence per 100-res unit
    if len(mytemplate) >= 100:
        for x in xrange(0, len(mytemplate), 100):                                                                 
            # string[0 : x] => string[(x-1)*100 : x]                                                            
            ########################################
            #print >> z, mytemplate[(x-100):x] + " " + str(x)                                      
            if x > 0:                                                                                              
                hold_lines.append(mytemplate[(x-100):x] + " " + str(x))                                     
                if len(mytemplate[x:]) > len(CDR_FR_string[x:]):
                    CDR_FR_lines.append(CDR_FR_string[(x-100):x] + "-"*(100-len(CDR_FR_string[(x-100):x])))
                else:
                    CDR_FR_lines.append(CDR_FR_string[(x-100):x])
                for _ in contigs_all: 
                    if (len(_) <= x + 5) and (len(_) >= x - 100):
                        #if re.search("^%s" % ('.'*x), _):
                        #print >> z, _[(x-100):]
                        hold_lines.append( _[(x-100):])
                ####################################################
                # last line of seq with < 100 residues
                char_left = len(mytemplate[x:])
                if char_left < 100:
                    #print >> z, mytemplate[x:] + " "*(100 - char_left + 1) +  str(len(mytemplate))
                    hold_lines.append(mytemplate[x:] + " "*(100 - char_left + 1) +  str(len(mytemplate)))
                    if len(mytemplate[x:]) > len(CDR_FR_string[x:]):
                        CDR_FR_lines.append(CDR_FR_string[x:] + "-"*(len(mytemplate[x:])- len(CDR_FR_string[x:])))
                    else:
                        CDR_FR_lines.append(CDR_FR_string[x:])
                    for _ in contigs_all:                          
                        if len(_) >= x:
                            #print >> z, _[x:]
                            hold_lines.append(_[x:])
    else: # len(mytemplate) < 100
        # first line is last line with seq length < 100 residues
        for x in xrange(0, len(mytemplate), 100):
            hold_lines.append(mytemplate[x:] + " "*(100 - len(mytemplate[x:]) + 1) +  str(len(mytemplate))) 
            if len(mytemplate[x:]) > len(CDR_FR_string[x:]):
                CDR_FR_lines.append(CDR_FR_string[x:] + "-"*(len(mytemplate[x:])- len(CDR_FR_string[x:])))
            else:
                CDR_FR_lines.append(CDR_FR_string[x:])
            for _ in contigs_all:                          
                if len(_) >= x:
                    hold_lines.append(_[x:])
            
    

    # final coverage checking
    prev_i = 0
    with open("%s_%s_StringentMatch.ali" % (denovoProgs, PDB_chain), 'w') as z:
        covered_index = []; flag = 0
        total_X = 0
        template_line_count = 0
        for i,_ in enumerate(hold_lines):
            _ = _.strip()
            if flag == 1: # found template line with last residue number
                for loc,cha in enumerate(_):
                    if cha != ".":
                        covered_index.append(int(loc))
                if i < len(hold_lines)-1:
                    if re.search("[0-9]", hold_lines[i+1]):                      
                        string = []
                        for temp_i, temp_x in enumerate(template_.split()[0]):
                            if not temp_i in covered_index:
                                #string.append(temp_x.lower())
                                string.append('X')
                                total_X += 1
                            else:
                                string.append('o')
                        if len(CDR_FR_lines[template_line_count]) > 0:
                            print >> z, CDR_FR_lines[template_line_count] + " FR/CDR"
                        else:
                            print >> z, '-'*len(template_) + " FR/CDR"
                        ###print >> zz, CDR_FR_lines[template_line_count]
                        template_line_count += 1
                        print >> z, ''.join(string)  + " coverage"
                        ###print >> zz, ''.join(string)  + " coverage"
                        #####################
                        noshort_lines = []
                        for hl in hold_lines[prev_i:i]:
                            if len(hl) > 2:
                                noshort_lines.append(hl)    
                        noshort_set = OrderedDict.fromkeys(noshort_lines).keys()
                        #print >> z, '\n'.join(hold_lines[prev_i:i])
                        print >> z, '\n'.join(noshort_set)
                        ###print >> zz, hold_lines[prev_i]
                        #####################
                        print >> z
                        print >> z
                        ###print >> zz
                        ###print >> zz
                        covered_index = []; flag = 0
                elif i == len(hold_lines) - 1:
                    string = []
                    for temp_i, temp_x in enumerate(template_.split()[0]):
                        if not temp_i in covered_index:
                            #string.append(temp_x.lower())
                            string.append('X')
                            total_X += 1
                        else:
                            string.append('o')
                    #print ''.join(string) + " " + str(template_.split()[1])
                    if len(CDR_FR_lines[template_line_count]) > 0:
                        print >> z, CDR_FR_lines[template_line_count] + " " * (100-len(template_.split()[0])+1) + "FR/CDR"
                    else:
                        print >> z, '-'*len(template_) + " " * (100-len(template_.split()[0])+1) + "FR/CDR"
                    ###print >> zz, CDR_FR_lines[template_line_count]
                    print >> z, ''.join(string)  + " " * (100-len(template_.split()[0])+1) + "coverage"
                    ###print >> zz, ''.join(string)  + " " * (100-len(template_.split()[0])+1) + "coverage"
                    #####################
                    noshort_lines = []
                    for hl in hold_lines[prev_i:i]:
                        if len(hl) > 2:
                            noshort_lines.append(hl)    
                    noshort_set = OrderedDict.fromkeys(noshort_lines).keys()
                    #####################
                    #print >> z, '\n'.join(hold_lines[prev_i:i])
                    print >> z, '\n'.join(noshort_set)
                    ###print >> zz, hold_lines[prev_i]
                    print >> z
                    print >> z
                    ###print >> zz
                    ###print >> zz
                    
            else: # not yet found template line with last residue number
                if re.search("[0-9]", _):
                    template_ = _
                    prev_i = i
                    flag = 1
                    
        print >> z, info

    ###zz.close()
        
        


        
def IQ_converter(seq, sample_pick):
    ######################################################    
    # i ==> I/L
    # q ==> Q/L

    #iq_converter = {'i':['I','L'], 'q':['Q','K']}
    iq_converter = {'i':['I','L']}
    mygroup_dic = defaultdict(list)
    #
    group_range = []
    for m in re.finditer(r'[i-q]', seq):
        group_range.append(m.start())
        mygroup_dic[m.start()].extend(iq_converter[seq[m.start()]])
    #
    nongroup_range = []
    for i,v in enumerate(seq):
        if not i in group_range:
            nongroup_range.append(i)
    for x in list(ranges(nongroup_range)):
        mygroup_dic[x[0]].append(seq[x[0]:x[1]+1])
    #    
    myrange = []
    for k in sorted(mygroup_dic.keys()):
        myrange.append("range(len(mygroup_dic[%d]))" % k)
    cmd = "product(%s)" % (','.join(myrange))
    # 
    seq_list = []
    for p in eval(cmd):
        var_str = []
        for i,k in enumerate(sorted(mygroup_dic.keys())):
            var_str.append(mygroup_dic[k][p[i]])
        mySeq_new = ''.join(var_str)
        seq_list.append(mySeq_new)

    if len(seq_list) > int(sample_pick):
        return sample(seq_list, sample_pick)
    else:
        return seq_list



def KABATnumbering4template(template, PDB_chain, chainID):


    if chainID in ["Hc", "Hv", "VH", "DH", "JH"]:
        chainID = "H"
    elif chainID in ["Lc", "Lv", "VK", "VL", "JK", "JL"]:
        chainID = "L"

    ###############################################
    # Heavy chain variable region - KABAT numbering
    ###############################################
    KABAT_VH_numbering = []
    for n in range(1, 113+1):
        Hnum = "H" + str(n)
        KABAT_VH_numbering.append(Hnum)
        if n == 35:
            for a in 'AB':
                Hnum_a = Hnum + a
                KABAT_VH_numbering.append(Hnum_a)
        elif n == 52:
            for a in 'ABC':
                Hnum_a = Hnum + a
                KABAT_VH_numbering.append(Hnum_a)
        elif n == 82:
            for a in 'ABC':
                Hnum_a = Hnum + a
                KABAT_VH_numbering.append(Hnum_a)
        elif n == 100:
            for a in 'ABCDEFGHIJK':
                Hnum_a = Hnum + a
                KABAT_VH_numbering.append(Hnum_a)

    KABAT_VH_regions = defaultdict(list)
    for k in KABAT_VH_numbering:
        m = re.match("H([0-9]*)[A-Z]?", k)
        if m:
            num = int(m.group(1))
            # HFR1 = H1-H30
            if num in range(1,30+1):
                KABAT_VH_regions['HFR1'].append(k)
            # HCDR1 = H31-H35B
            elif num in range(31,35+1):
                KABAT_VH_regions['HCDR1'].append(k)
            # HFR2 = H36-H49
            elif num in range(36,49+1):
                KABAT_VH_regions['HFR2'].append(k)
            # HCDR2 = H50-H65
            elif num in range(50,65+1):
                KABAT_VH_regions['HCDR2'].append(k)
            # HFR3 = H66-H94
            elif num in range(66,94+1):
                KABAT_VH_regions['HFR3'].append(k)
            # HCDR3 = H95-H102
            elif num in range(95,102+1):
                KABAT_VH_regions['HCDR3'].append(k)
            # HFR4 = H103-H113
            elif num in range(103,113+1):
                KABAT_VH_regions['HFR4'].append(k)

    #for k,v in sorted(KABAT_VH_regions.iteritems(), key=lambda (k,v):(int(v[0].replace("H","")),k)):
    #    print k,v
                
        
    ###############################################
    # Light chain variable region - KABAT numbering
    ###############################################
    KABAT_VL_numbering = []
    for n in range(1, 109+1):
        Lnum = "L" + str(n)
        KABAT_VL_numbering.append(Lnum)
        if n == 27:
            for a in 'ABCDEF':
                Lnum_a = Lnum + a
                KABAT_VL_numbering.append(Lnum_a)
        elif n == 95:
            for a in 'ABCDEF':
                Lnum_a = Lnum + a
                KABAT_VL_numbering.append(Lnum_a)
        elif n == 106:
            for a in 'A':
                Lnum_a = Lnum + a
                KABAT_VL_numbering.append(Lnum_a)
    #print KABAT_VL_numbering

    KABAT_VL_regions = defaultdict(list)
    for k in KABAT_VL_numbering:
        m = re.match("L([0-9]*)[A-Z]?", k)
        if m:
            num = int(m.group(1))
            # LFR1 = L1-L23
            if num in range(1,23+1):
                KABAT_VL_regions['LFR1'].append(k)
            # LCDR1 = L24-L34
            elif num in range(24,34+1):
                KABAT_VL_regions['LCDR1'].append(k)
            # LFR2 = H35-L49
            elif num in range(35,49+1):
                KABAT_VL_regions['LFR2'].append(k)
            # LCDR2 = H50-H56
            elif num in range(50,56+1):
                KABAT_VL_regions['LCDR2'].append(k)
            # LFR3 = H56-H88
            elif num in range(56,88+1):
                KABAT_VL_regions['LFR3'].append(k)
            # LCDR3 = L89-L97
            elif num in range(89,97+1):
                KABAT_VL_regions['LCDR3'].append(k)
            # LFR4 = H98-L109
            elif num in range(98,109+1):
                KABAT_VL_regions['LFR4'].append(k)

    #for k,v in sorted(KABAT_VL_regions.iteritems(), key=lambda (k,v):(int(v[0].replace("L","")),k)):
    #    print k,v
    #sys.exit()
        

    #print chainID
    KABATnumbering_fas = '/tmp/ipark/KABAT_%s.fas' % PDB_chain.replace(".fas", "")
    cmd = 'lynx --dump "http://www.bioinf.org.uk/cgi-bin/abnum/abnum.pl?plain=1&aaseq=%s&scheme=-k"  > %s' % \
(template, KABATnumbering_fas)
    #print cmd
    #print
    posix.system(cmd)

    temp_dic = {}
    with open(KABATnumbering_fas, 'r') as f:
        for _ in f:
            m = re.match("^(%s[0-9]*[A-Z]?)\s+([A-Z])" % chainID, _)
            if m:
                KABAT_num = m.group(1)
                KABAT_res = m.group(2)
                temp_dic[KABAT_num] = KABAT_res

    temp_string = []; FR_CDR = [] 
    CDR1_ = "CDR1"; CDR2_ = "CDR2"; CDR3_ = "CDR3"; FR_ = "frameregion"

    if chainID == 'H':
    ######################
        for k,v in sorted(KABAT_VH_regions.iteritems(), key=lambda (k,v):(int(v[0].replace("H","")),k)):
            temp_region = []
            for ii, vv in enumerate(v):
                if temp_dic.has_key(vv):
                    if re.search("CDR1", k):
                        FR_CDR.append(CDR1_[ii%4])
                    elif re.search("CDR2", k):
                        FR_CDR.append(CDR2_[ii%4])
                    elif re.search("CDR3", k):
                        FR_CDR.append(CDR3_[ii%4])
                    elif re.search("FR", k): 
                        FR_CDR.append(FR_[ii%11])
                    temp_string.append(temp_dic[vv])
                    temp_region.append(temp_dic[vv])

    elif chainID == 'L':
    ######################
        for k,v in sorted(KABAT_VL_regions.iteritems(), key=lambda (k,v):(int(v[0].replace("L","")),k)):
            temp_region = []
            for ii, vv in enumerate(v):
                if temp_dic.has_key(vv):
                    if re.search("CDR1", k):
                        FR_CDR.append(CDR1_[ii%4])
                    elif re.search("CDR2", k):
                        FR_CDR.append(CDR2_[ii%4])
                    elif re.search("CDR3", k):
                        FR_CDR.append(CDR3_[ii%4])
                    elif re.search("FR", k): 
                        FR_CDR.append(FR_[ii%11])
                    temp_string.append(temp_dic[vv])
                    temp_region.append(temp_dic[vv])
    ###print ''.join(FR_CDR)
    return ''.join(FR_CDR)
        
    #posix.system('rm %s' % KABATnumbering_fas)
    

def AlignRead_ConfLevel(template, denovoProgs, PDB_chain, info):

    align_file = "%s_%s_StringentMatch.ali" % (denovoProgs, PDB_chain)
    template_lines = []; coverage_lines = []; variable_lines = []
    with open(align_file, 'r') as f:
        for line in f:
            if re.search(r'^[A-N,P-W,Y,a-n,p-w,y]*\s+[0-9][0-9]*', line):  
                template_lines.append(line)    
            elif re.search(r'\s+coverage', line):
                coverage_lines.append(line)    
            elif re.search(r'\s+FR/CDR', line):
                variable_lines.append(line)    

    #print template_lines, coverage_lines, variable_lines

    flag = 0
    count = 0
    conflevel_dic = defaultdict(int)
    with open(align_file, 'r') as f:
        for _ in f:
            if flag > 0:                            
                if re.search(r'^$', _):
                    flag = 0
                else:
                    for dind, dres in enumerate(_):                    
                        dind = dind + 1 + (count-1)*100
                        if dres.isalpha():
                            conflevel_dic[dind] += 1                  
            else:    
                if re.search(r'^[A-N,P-W,Y,a-n,p-w,y]*\s+[0-9][0-9]*', _):
                    #print count*100, _
                    flag = 1; count += 1
                elif re.search(r'^===', _):
                    break
    #print conflevel_dic

    zz = open(align_file.replace(".ali", ".seq"), 'w')
    for indt, t in enumerate(template_lines):
        count_array = []
        print >> zz, variable_lines[indt],
        print >> zz, coverage_lines[indt],
        print >> zz, t,
        curr_range = range(indt*100+1, int(t.split()[1])+1)
        max_freq = max([conflevel_dic[j] for j in curr_range])
        for freq in range(1, max_freq+1): 
            count_array = [' ' for f in curr_range]
            for indx, x in enumerate(curr_range):
                if conflevel_dic[x] >= freq:
                    count_array[indx] = '*'
            print >> zz, ''.join(count_array)
        

    print >> zz
    print >> zz
    print >> zz, info

    zz.close()
        
    
def ReadContigList(contigs_file):
    contigs_list = []
    for cf in contigs_file:
        with open(cf, 'r') as f:
            for _ in f:
                _ = _.strip()
                _ = _.replace("i", "L")
                _ = _.replace("q", "Q")
                _ = _.replace("I", "L")
                _ = _.replace("K", "Q")
                if len(_.split()) == 2:
                    if re.search("...1", _.split()[1]):
                        contigs_list.append(_.split()[0])
                    elif re.search("[0-9]*", _.split()[0]):
                        contigs_list.append(_.split()[1])
                else:
                    contigs_list.append(_)

    contigs_set = OrderedDict.fromkeys(contigs_list).keys()
    return contigs_set

def CDR_builder_from_SeedFR(contigs_set, SeedFR, nextSeedFR, ter_overlap, file_prefix):
    
    SeedFR = SeedFR.replace("I", "L") 
    SeedFR = SeedFR.replace("K", "Q") 
    print
    print "[SeedFR]"
    print SeedFR

    # a: contig      cccccccc
    # b: seedFR ssssssss

    align_contigs = []
    seed_ = SeedFR
    for contig in contigs_set:                                           
        s = difflib.SequenceMatcher(None, contig, seed_[-ter_overlap-1:])
        t = s.get_matching_blocks()
        aindex = t[0][0]; bindex = t[0][1]; overlap = t[0][2]
        if aindex == 0 and (overlap >= ter_overlap):
            #prefix = '.'*(len(seed_) - ter_overlap)
            prefix = seed_[:len(seed_) - ter_overlap]
            prefix_contig = prefix + contig
            if len(prefix_contig) > len(seed_):
                align_contigs.append(prefix_contig)
    align_contigs = OrderedDict.fromkeys(align_contigs).keys() 
    align_contigs.sort(key=lambda line:len(line))
    #print '\n'.join(align_contigs)
    #print SeedFR
    #print "========================================"


    for nest_level in range(5):
        if nest_level == 0:
            align_contigs_in = align_contigs
        else:
            align_contigs_in = align_contigs_out

        outfilename = file_prefix + '_nest' + str(nest_level + 1) + '.out'
        NextFR_check(align_contigs_in, nextSeedFR, ter_overlap, outfilename)
        align_contigs_out = nested_growth(align_contigs_in, ter_overlap) 


def FRbase_align(FRbase, contigs_set):
    FRbase = FRbase.replace("I", "L") 
    FRbase = FRbase.replace("K", "Q") 
    found_list = []
    #print FRbase
    #print "-"*len(FRbase)
    found_list.append(FRbase)
    found_list.append("-"*len(FRbase))
    #for contig_ in contigs_set:
    for ter_overlap in xrange(len(FRbase)/2, 2, -1):
        for contig_ in contigs_set:
            #                                       a                  b              
            s = difflib.SequenceMatcher(None, FRbase[-ter_overlap:], contig_)
            t = s.get_matching_blocks()
            aindex = t[0][0]; bindex = t[0][1]; overlap = t[0][2] 
            if overlap >= ter_overlap:
                #print '.'*(len(FRbase) - ter_overlap - bindex) + contig_
                found_list.append('.'*(len(FRbase) - ter_overlap - bindex) + contig_)
    
    found_list = OrderedDict.fromkeys(found_list).keys() 
    outfilename = FRbase + "_DenovoAlign.out"
    print_growth(found_list, outfilename)



def NextFR_check(align_contigs, nextSeedFR, ter_overlap, outfilename):
    nextSeedFR = nextSeedFR.replace("I", "L") 
    nextSeedFR = nextSeedFR.replace("K", "Q") 
    found_list = []
    for contig_ in align_contigs:
        s = difflib.SequenceMatcher(None, nextSeedFR, contig_[-ter_overlap:])
        t = s.get_matching_blocks()
        aindex = t[0][0]; bindex = t[0][1]; overlap = t[0][2]
        if aindex == 0 and (overlap >= ter_overlap):
            found_list.append(contig_)
    if len(found_list) > 0:
        print '\n'.join(found_list)               
        print '[nextSeedFR]'
        print '.'*(len(found_list[-1])-ter_overlap) + nextSeedFR            
        print_growth(align_contigs, outfilename)
        sys.exit()
    else:
        print
        print '   <continue nest_growth>'
        print_growth(align_contigs, outfilename)

def print_growth(align_contigs, outfilename):
    with open(outfilename, 'w') as f:
        align_contigs.sort(key=lambda line:len(line)) 
        print >> f, '\n'.join(align_contigs)
    

def nested_growth(align_contigs, ter_overlap):

    # a: contig      cccccccc
    # b: seed_ ssssssss

    align_contigs2 = []
    for seed_ in align_contigs:
        for contig in contigs_set:                                            
            s = difflib.SequenceMatcher(None, contig, seed_[-ter_overlap-1:])
            t = s.get_matching_blocks()
            aindex = t[0][0]; bindex = t[0][1]; overlap = t[0][2]
            if aindex == 0 and (overlap >= ter_overlap):
                #prefix = '.'*(len(seed_) - ter_overlap)
                prefix = seed_[:len(seed_) - ter_overlap]
                prefix_contig = prefix + contig
                if len(prefix_contig) > len(seed_):
                    #if not seed_ in align_contigs2:
                    #    align_contigs2.append(seed_)
                    align_contigs2.append(prefix_contig)
    align_contigs2 = OrderedDict.fromkeys(align_contigs2).keys() 
    #align_contigs2.sort(key=lambda line:len(line))
    #print '\n'.join(align_contigs2)
    #print SeedFR
    #print "========================================"
    return align_contigs2
        

def get_sequence_index(template_portion, fp):
    s = difflib.SequenceMatcher(None, template_portion, fp)
    t = s.get_matching_blocks()
    ls = t[0][0]; le = t[0][0]+t[0][2]-1  # as it's index, starting from 0
    return ls, le


def get_sequence_ratio(template_portion, fp):
    template_portion = template_portion.replace("I", "L")
    template_portion = template_portion.replace("K", "Q")
    s = difflib.SequenceMatcher(None, template_portion, fp)
    sim = s.ratio()
    return sim


def find_patterns_onto_denovoseq(text_list, template, re_pattern, rexp_s):

    mylist_dic = {}
    for text in text_list:
        re_pattern = re_pattern.replace("I", "L")
        re_pattern = re_pattern.replace("K", "Q")
        for match in re.finditer(re_pattern, text):
            text_s = match.start()                                        
            text_e = match.end()
            
            if rexp_s <= text_s:
                ##local_s = text_s - rexp_s + 1
                local_s = text_s - rexp_s
                #
                before_mat = text[local_s:text_s].lower()
                over_mat = text[text_s:text_e].upper()
                after_mat = text[text_e:].lower()
                join_str = before_mat + over_mat + after_mat
                get_val = get_sequence_ratio(template[local_s:local_s+len(text)], text)
                if get_val > 0.8:
                    sim = '{:3.2f}'.format(get_val)
                    mystr = '%s' % (join_str) 
                    mylist_dic[mystr] = sim
            else:
                ##prefix = '.' * (rexp_s - text_s - 1) # -1 for backquote
                prefix = '.' * (rexp_s - text_s) # -1 for backquote
                #
                before_mat = text[:text_s].lower()
                over_mat = text[text_s:text_e].upper()
                after_mat = text[text_e:].lower()
                join_str = before_mat + over_mat + after_mat
                get_val = get_sequence_ratio(template[rexp_s-text_s:rexp_s-text_s+len(text)], text)
                if get_val > 0.8:
                    sim = '{:3.2f}'.format(get_val)
                    mystr = '%s%s' % (prefix, join_str) 
                    mylist_dic[mystr] = sim
    print_lines = []
    conf_score = 0.0
    for k,v in sorted(mylist_dic.iteritems(), key=lambda (k,v):(float(v),k), reverse=True):
        conf_score += float(v)
        string = k + ' (' + v + ')'
        print_lines.append(string)
    
    return print_lines, conf_score
    


def MovingConfidenceScore(contigs_set, mytemplate, consecutive_num, denovoProgs, CDR_FR_string, PDB_chain, chainID, info):
    ###########################################################################################

    PDB_chain = PDB_chain.replace(".fas", "")
    hold_lines = []
    conflevel_dic = defaultdict(int)
    ############################################################################# forward
    ############################################################################# forward
    ############################################################################# forward
    StartingSeq = mytemplate[:consecutive_num]; RemainingSeq = mytemplate[consecutive_num:len(mytemplate)]
                                                                                                                            
    FR_patterns = []
    for i,x in enumerate(RemainingSeq):
        FR_patterns.append(StartingSeq + RemainingSeq[:i])
    FR_patterns.append(StartingSeq + RemainingSeq)
                                                                                                                            
    #print >> z, PDB_chain + " forward"
    #print >> z, ">"*(len(PDB_chain) + len(" forward"))
    #hold_lines.append(">"*len(mytemplate) + " " + PDB_chain + " forward")
    hold_lines.append("|>>>>"*(len(mytemplate)/5) + " " + PDB_chain + " forward")
    
                                                                                                                        
    Fconflevel_dic = {}
                                                                                                                        
    tindex_e = consecutive_num - 1
    for pattern in FR_patterns:                                                                       
        #tindex_s, tindex_e = get_sequence_index(mytemplate, pattern)
        re_pattern = pattern[-consecutive_num:]
        #rindex_s, rindex_e = get_sequence_index(mytemplate, re_pattern)
        rindex_s = tindex_e - consecutive_num + 1
        print_lines, conf_score = find_patterns_onto_denovoseq(contigs_set, mytemplate, re_pattern, rindex_s)
        Fconflevel_dic[tindex_e] = conf_score 
        tindex_e += 1
        ###+
        ####+ forward growth, key = last index of template matching to pattern
        ######+
        #######+
                                                                                                                        
    Fmax_freq = max([int(math.ceil(Fconflevel_dic[j]/10.)) for j in range(len(mytemplate)) if Fconflevel_dic.has_key(j)])

    # histogram up
    count_array = []
    for freq in reversed(range(1, Fmax_freq + 1)):
        count_array = ['.' for f in range(len(mytemplate))]
        for x in range(len(mytemplate)):
            if Fconflevel_dic.has_key(x):
                if Fconflevel_dic[x] >= freq: 
                    count_array[x] = '*'
                    conflevel_dic[x] += 1
            else:
                if freq == 1:
                    count_array[x] = 'x'
        #print >> z, ''.join(count_array)
        hold_lines.append(''.join(count_array))
    
    ############################################################################# backward
    ############################################################################# backward
    ############################################################################# backward
    StartingSeq = mytemplate[-consecutive_num:]; RemainingSeq = mytemplate[:len(mytemplate)-consecutive_num]
                                                                                                                            
    FR_patterns = []
    FR_patterns.append(StartingSeq)
    for i,x in enumerate(RemainingSeq[::-1]):
        FR_patterns.append(RemainingSeq[-i-1:] + StartingSeq)
                                                                                                                            
    #print >> z, mytemplate.strip()[:len(mytemplate)] + ' ' + str(len(mytemplate))
    hold_lines.append(mytemplate.strip()[:len(mytemplate)] + ' ' + str(len(mytemplate)))
    if len(CDR_FR_string) == 0:
        hold_lines.append('-'*len(mytemplate) + ' FR/CDR')
    else: # len(CDR_FR_string) > 0
        if len(CDR_FR_string) >= len(mytemplate):
            hold_lines.append(CDR_FR_string + ' FR/CDR')
        else: # len(CDR_FR_string) < len(mytemplate)
            hold_lines.append(CDR_FR_string + '-'*(len(mytemplate)-len(CDR_FR_string)) + ' FR/CDR')
    #print >> z, mytemplate.strip()[:len(mytemplate)] + ' ' + str(len(mytemplate))
    hold_lines.append(mytemplate.strip()[:len(mytemplate)] + ' ' + str(len(mytemplate)))

    Bconflevel_dic = {}
                                                                                                                        
    tindex_s = len(mytemplate) - consecutive_num
    for pattern in FR_patterns:                                                                       
        #tindex_s, tindex_e = get_sequence_index(mytemplate, pattern)
        re_pattern = pattern[:consecutive_num]
        #rindex_s, rindex_e = get_sequence_index(mytemplate, re_pattern)
        rindex_s = tindex_s
        print_lines, conf_score = find_patterns_onto_denovoseq(contigs_set, mytemplate, re_pattern, rindex_s)
        Bconflevel_dic[tindex_s] = conf_score ################# essential with key=tindex_s
        tindex_s -= 1
        #                           +### 
        #  backward growth,        +####
        #  key = starting index   +#####
        #                        +######
    Bmax_freq = max([int(math.ceil(Bconflevel_dic[j]/10.)) for j in range(len(mytemplate)) if Bconflevel_dic.has_key(j)])

    # histogram down
    count_array = []
    for freq in range(1, Bmax_freq + 1):
        count_array = ['.' for f in range(len(mytemplate))]
        for x in range(len(mytemplate)):
            if Bconflevel_dic.has_key(x):
                if Bconflevel_dic[x] >= freq: 
                    count_array[x] = '*'
                    conflevel_dic[x] += 1
            else:
                if freq == 1: 
                    count_array[x] = 'x'
        #print >> z, ''.join(count_array)
        hold_lines.append(''.join(count_array))
    
    #print >> z, PDB_chain + " backward"
    #print >> z, "<"*(len(PDB_chain) + len(" backward"))
    #hold_lines.append("<"*len(mytemplate) + " " + PDB_chain + " backward")
    hold_lines.append("|<<<<"*(len(mytemplate)/5) + " " + PDB_chain + " backward")
    #print >> z

    ############################################################################# foward+backward
    ############################################################################# foward+backward
    ############################################################################# foward+backward


    ##################################
    ## text wrapping #################
    ##################################
    ConfLevel_filename = "%s_%s_EvolvingTemplate.out" % (denovoProgs, PDB_chain.replace(".fas", ""))
    if len(mytemplate) <= 125:
        with open(ConfLevel_filename, 'w') as z: 
            print >> z, info
            print >> z
            print >> z, '\n'.join(hold_lines)
    else:
        # print full sequence per 100-res unit                                                                          
        prev_x = 0
        with open(ConfLevel_filename, 'w') as z:
            print >> z, info
            print >> z
            for x in xrange(0, len(mytemplate), 125):                    
               if x > 0:
                    for i,_ in enumerate(hold_lines):                   
                        _ = _.strip()
                        if re.search("<<<", _):
                            print >> z, _[prev_x:x] + " " + PDB_chain + " backward\n"
                            last_i = i
                            break
                        elif re.search(">>>", _):
                            print >> z, _[prev_x:x] + " " + PDB_chain + " forward"
                        else:
                            if not re.search("\*|\/|_", _):
                                print >> z, _[prev_x:x] + " %d" % x
                            else:
                                print >> z, _[prev_x:x] 
                    prev_x = x
                    ####################################################
                    # last line of seq with < 100 residues
                    char_left = len(mytemplate[x:])
                    if char_left < 125:
                        for i,_ in enumerate(hold_lines):
                            _ = _.strip()
                            if i <= last_i:
                                print >> z, _[x:]

    #return conflevel_dic
    return ConfLevel_filename



def find_patterns_onto_denovoseq_AA20candidate(text_list, template, re_pattern, rexp_s, rexp_e):

    mylist_dic = {}
    for text in text_list:
        re_pattern = re_pattern.replace("I", "L")
        re_pattern = re_pattern.replace("K", "Q")
        for match in re.finditer(re_pattern, text):
            text_s = match.start()                                        
            text_e = match.end()
            
            if rexp_s <= text_s:
                ##local_s = text_s - rexp_s + 1
                local_s = text_s - rexp_s
                #
                before_mat = text[local_s:text_s].lower()
                over_mat = text[text_s:text_e].upper()
                after_mat = text[text_e:].lower()
                join_str = before_mat + over_mat + after_mat
                get_val = get_sequence_ratio(template[local_s:local_s+len(text)], text)
                if get_val > 0.8:
                    sim = '{:3.2f}'.format(get_val)
                    mystr = '%s' % (join_str) 
                    mylist_dic[mystr] = sim
            else:
                ##prefix = '.' * (rexp_s - text_s - 1) # -1 for backquote
                prefix = '.' * (rexp_s - text_s) # -1 for backquote
                #
                before_mat = text[:text_s].lower()
                over_mat = text[text_s:text_e].upper()
                after_mat = text[text_e:].lower()
                join_str = before_mat + over_mat + after_mat
                get_val = get_sequence_ratio(template[rexp_s-text_s:rexp_s-text_s+len(text)], text)
                if get_val > 0.8:
                    sim = '{:3.2f}'.format(get_val)
                    mystr = '%s%s' % (prefix, join_str) 
                    mylist_dic[mystr] = sim
    print_lines = []
    conf_score = 0.0
    for k,v in sorted(mylist_dic.iteritems(), key=lambda (k,v):(float(v),k), reverse=True):
        conf_score += float(v)
        string = k + ' (' + v + ')'
        print_lines.append(string)
    
    return print_lines, conf_score
    


def MovingConfidenceScore_20AAcandidate(contigs_set, SeedFR):
    ###########################################################################################
    """
    template_VL forward
    =========================>
         ******************                 ************       *****    * * ****************       *****************
         ******************                 ************       *****    * * **************** *     *****************
        *******************                *************       *****    * * **************** **    *****************
        *******************                *************       ****** * * * **************** ***   *****************
        *******************                **************      ****** *** * **************** ***   *****************
        ********************               **************      ********** * **************** ***   *****************
        ********************         *     **************      ********** * **************** ***   *****************
       **********************       ***   ***************   ** ********** * ********************   *****************
       **********************  **   ***   ***************   ************* * ********************** *****************
    --*********************** **** *****  ***************   ************* * ****************************************
    DIQMTQSPSSLSASVGDRVTITCRASQDIPRSISGYVAWYQQKPGKAPKLLIYWGSYLYSGVPSRFSGSGSGTDFTLTISSLQPEDFATYYCQQHYTTPPTFGQGTKVEIKRT 113
    frameregionframeregionfCDR1CDR1R1CDR1CframeregionframCDR2CDRframeregionframeregionframeregioCDR3CDRDRframereginfr FR/CDR
    DIQMTQSPSSLSASVGDRVTITCRASQDIPRSISGYVAWYQQKPGKAPKLLIYWGSYLYSGVPSRFSGSGSGTDFTLTISSLQPEDFATYYCQQHYTTPPTFGQGTKVEIKRT 113
    ********************** **** *****  ***************   ************* * *******************************************
    **********************  **   ***   ***************   ************* * ********************** ********************
    **********************       ***   ***************   ** ********** * ********************   ********************
     ********************         *     **************      ********** * **************** ***   ********************
     ********************               **************      ********** * **************** ***   ********************
     *******************                **************      ****** *** * **************** ***   ********************
     *******************                *************       ****** * * * **************** ***   ********************
     *******************                *************       *****    * * **************** **    ********************
      ******************                 ************       *****    * * **************** *     ********************
      ******************                 ************       *****    * * ****************       ********************
                                                                                        <=========================
                                                                                          template_VL backward
    known vs. template
    ==================
    DIQMTQSPSSLSASVGDRVTITCRASQDVN----TAVAWYQQKPGKAPKLLIYSASFLYSGVPSRFSGSRSGTDFTLTISSLQPEDFATYYCQQHYTTPPTFGQGTKVEIKRTVAAPSVF known_LC                     
    DIQMTQSPSSLSASVGDRVTITCRASQDIPRSISGYVAWYQQKPGKAPKLLIYWGSYLYSGVPSRFSGSGSGTDFTLTISSLQPEDFATYYCQQHYTTPPTFGQGTKVEIKRTVAAPSVF gi|213424087|pdb|3BDY|L      
    ****************************:       ***************** .*:************ **************************************************
    """

    conflevel_dic = {}                                                                                                           
                                                                                                                                 
    AA20candidate = AA_1to3.keys()
    AA20candidate.sort()
                                                                                                                                 
    for j in AA20candidate:                                                                                                      
        template = SeedFR + j
        re_pattern = template[-4:]
        rexp_s, rexp_e = get_sequence_index(template, re_pattern)
        print_lines, conf_score = find_patterns_onto_denovoseq_AA20candidate(contigs_set, template, re_pattern, rexp_s, rexp_e)
        #print template
        #print '\n'.join(print_lines)
        conflevel_dic[template] = conf_score 
        ###+
        ####+ forward growth, key = last index of template matching to pattern
        ######+
        #######+
    print
    print SeedFR
    print '='*len(SeedFR)
    for k,v in sorted(conflevel_dic.iteritems(), key=lambda (k,v):(v, k)):
        if v > 0.0:
            print k, v
    

def I2L_K2Q_str(seed):
    seed = seed.replace("I", "L")
    seed = seed.replace("K", "Q")
    return seed



def FRbased_CDRgrowth():
    ###########################################################################################
    """
    FRbase = "QSPSSLSASVGDRVTITCRA"
    FRbase_align(FRbase, contigs_set)
    # build CDR1
    ter_overlap = 3
    #SeedFR1 = "EVQLVESGGGLVQPGGSLRLSCAASG" 
    #SeedFR2 = "VRQAPGKGLEWV"               
    SeedFR1 = "QSPSSLSASVGDRVTITCRA"
    SeedFR2 = "WYQQKPGKAP"
    file_prefix = "VL_SeedFR1_buildCRD1"
    CDR_builder_from_SeedFR(contigs_set, SeedFR1, SeedFR2, ter_overlap, file_prefix)
    """
    """
    [SeedFR]
    EVQLVESGGGLVQPGGSLRLSCAASG
    
       <continue nest_growth>
    EVQLVESGGGLVQPGGSLRLSCAASGFNLQDTYLHWVRQ
    [nextSeedFR]
    ....................................VRQAPGQGLEWV (OK)
    """

    # build CDR2
    """
    ter_overlap = 3
    #SeedFR2 = "VRQAPGKGLEWV"               
    SeedFR2 = "VRQAPGQGLEWVARLYPTNGYTRYADSVQG"
    SeedFR3 = "TAYLQMNSLRAEDTAVYYC"        
    file_prefix = "SeedFR2_buildCRD2"
    CDR_builder_from_SeedFR(contigs_set, SeedFR2, SeedFR3, ter_overlap, file_prefix)
    """

    # build CDR3
    """
    ter_overlap = 3
    SeedFR3 = "TAYLQMNSLRAEDTAVYYC"        
    SeedFR4 = "WGQG"                      
    file_prefix = "SeedFR3_buildCRD3"
    CDR_builder_from_SeedFR(contigs_set, SeedFR3, SeedFR4, ter_overlap, file_prefix)
    """
    """
    [SeedFR]
    TAYLQMNSLRAEDTAVYYC
    
       <continue nest_growth>
    
       <continue nest_growth>
    TAYLQMNSLRAEDTAVYYCSRWGGDGFYAMDYWGQ
    [nextSeedFR]
    ................................WGQG (OK)
    """
    ###########################################################################################


def main_Whole_Workflow_KABATpdbaa():


    ###########################################################################################
    #### 0st: blast search ####################################################################
    ###########################################################################################

    denovoProgs = "distiller"
    ##contigs_file = ["7-16-4_Fab_denovo_tags.list"]
    ##prefix = "7-16-4_Fab_denovo_tags_ClustMerge1"
    contigs_file = ["../../step1/output/2H2_pepsin_tags.list", "../../step1/output/2H2_trypsin_chymotrypsin_elastase_tags.list"]
    prefix = "2H2_longTags_Merge"
    

    consecutive_num = 3
    denovo_fas_file = prefix + ".fas"
    ###for DB in ["KABAT1999", "pdbaa"]:
    for DB in ["KABAT1999"]:
        blast_result_HL = "%s_%s_blastp.out" % (prefix, DB)
        cmd = "blastp -query %s.fas -db ~/Progs/blastplus/db/%s -out %s  -outfmt \
'6 std qseq sseq' -num_threads 8 -evalue 1e-2" % (prefix, DB, blast_result_HL)
        posix.system(cmd)
        print cmd
        
    
        ######################################################################################### 
        #### 1st: check out robustness of template ##############################################
        #### 2nd: put the template sequence for Hv and Lv #######################################
        #### 3rd: align denovo sequence onto the template via stringent matching ################
        #### 4th: align denovo sequence onto the template via evolving template  ################
        #########################################################################################

        if DB == "KABAT1999":
            chains = ["Hv", "Lv"]
            #chains = ["Hv", "Hc", "Lv", "Lc"]
        elif DB == "pdbaa":
            chains = ["H", "L"]

        for chainID in chains:
            blast_result = blast_result_HL.replace(".out", "_%s.out" % chainID)
            cmd2 = "cat %s | sort -k 9 -n | grep '|%s'  > %s; sleep 2" % (blast_result_HL, chainID, blast_result)
            posix.system(cmd2)

            template_list = blast_coverage(denovo_fas_file, blast_result, DB)
            contigs_set = ReadContigList(contigs_file)

            for PDB_chain in template_list:   
                info = """
======================================================================
  denovoseq generator = %s
  Blast DB search with 
  ....merged_denovoseq = %s.fas 
  ....DB = %s
  Contigs mapping 
  ....onto the template = %s
  ....with denovoseq = %s
======================================================================
""" % (denovoProgs, prefix, DB, PDB_chain, contigs_file)
                print info                                                                                                              
                mytemplate, CDR_FR_string = get_TemplateSeq_CDRFRstring_from_accession(PDB_chain, chainID)             
                MovingConfidenceScore(contigs_set, mytemplate, consecutive_num, denovoProgs, CDR_FR_string, PDB_chain, chainID, info)

            if chainID == "H":
                cmd1 = "for x in distiller_*_H_*out; do y=`cat -n $x | grep backward | head -n1 | awk '{print $1}'`; head -n$y $x > $x.125; done"
                cmd2 = "for x in distiller*_H_*out.125; do y=`tr -cd \* < $x | wc -c`; z=`tr -cd - < $x | wc -c`; echo $z $y $x; done  | egrep -v '^1[0-9][0-9]' | sort -k 2 -n | awk '{print $3}' | tail -n2  | while read line; do cat $line; done > HC.top2"
                posix.system(cmd1)
                posix.system(cmd2)
            elif chainID == "L":
                cmd1 = "for x in distiller_*_L_*out; do y=`cat -n $x | grep backward | head -n1 | awk '{print $1}'`; head -n$y $x > $x.125; done"
                cmd2 = "for x in distiller*_L_*out.125; do y=`tr -cd \* < $x | wc -c`; z=`tr -cd - < $x | wc -c`; echo $z $y $x; done  | egrep -v '^1[0-9][0-9]' | sort -k 2 -n | awk '{print $3}' | tail -n2  | while read line; do cat $line; done > LC.top2"
                posix.system(cmd1)
                posix.system(cmd2)
            else:
                cmd = "for x in `ls distiller_*_%s_*out`; do y=`tr -cd \* < $x | wc -c`; echo $y $x; done | sort -k 1 -n | tail -n2 | while read line; do cat $line; done > %s.top2" % (chainID, chainID)
                posix.system(cmd)


def main_Whole_Workflow_NCBIgermline():

    ###########################################################################################
    #### 0st: blast search ####################################################################
    ###########################################################################################

    denovoProgs = "distiller"
    contigs_file = ["../../step1/output/2H2_pepsin_tags.list", "../../step1/output/2H2_trypsin_chymotrypsin_elastase_tags.list"]


    DB = "NCBIgermline_pr"
    consecutive_num = 3
    prefix = "2H2_longTags_Merge"
    denovo_fas_file = prefix + ".fas"
    blast_result_HL = "%s_%s_blastp.out" % (prefix, DB)
    cmd = "blastp -query %s.fas -db ~/Progs/blastplus/db/%s -out %s  -outfmt \
'6 std qseq sseq' -num_threads 8 -evalue 1e-2" % (prefix, DB, blast_result_HL)
    print cmd
    posix.system(cmd)
    
    ######################################################################################### 
    #### 1st: check out robustness of template ##############################################
    #### 2nd: put the template sequence for Hv and Lv #######################################
    #### 3rd: align denovo sequence onto the template via stringent matching ################
    #### 4th: align denovo sequence onto the template via evolving template  ################
    #########################################################################################

    #seq1254	gnl|blast|mVH1_J558.62pg.158	51.61	31	15	0	11	41	7	99	0.002	30.4	QSQQPGAELVQPMGSSPESSQFLVALFFSTG	QLQQPGAELVKPGASVKLSARLLATLSPATG
    #chains = ["VH", "DH", "JH", "VK", "VL", "JK", "JL"]
    #chains = ["VK", "VL", "VH", "DH", "JH", "JK", "JL"]
    chains = ["JK", "JL"]

    for chainID in chains:
        blast_result = blast_result_HL.replace(".out", "_%s.out" % chainID)
        print blast_result
        cmd2 = "cat %s | sort -k 9 -n | egrep -e [h,m]%s  > %s" % (blast_result_HL, chainID, blast_result)
        posix.system(cmd2)

        template_list = blast_coverage(denovo_fas_file, blast_result, DB)
        contigs_set = ReadContigList(contigs_file)

        for PDB_chain in template_list:   
            info = """
======================================================================
  denovoseq generator = %s
  Blast DB search with 
  ....merged_denovoseq = %s.fas 
  ....DB = %s
  Contigs mapping 
  ....onto the template = %s
  ....with denovoseq = %s
======================================================================
""" % (denovoProgs, prefix, DB, PDB_chain, contigs_file)
            print info                                                                                                              
            mytemplate, CDR_FR_string = get_TemplateSeq_CDRFRstring_from_accession_NCBIgermline(PDB_chain, chainID)             
            PDB_chain = PDB_chain.replace("/", "-")
            MovingConfidenceScore(contigs_set, mytemplate, consecutive_num, denovoProgs, CDR_FR_string, PDB_chain, chainID, info)



    cmd1="for x in `ls distiller_*out | egrep -e 'VH|DH|JH'`; do y=`tr -cd \* < $x | wc -c`; echo $y $x; done | sort -k 1 -n | tail -n2 | while read line; do cat $line; done > VH.top2"
    cmd2="for x in `ls distiller_*out | egrep -e 'VK|VL|JK|JL'`; do y=`tr -cd \* < $x | wc -c`; echo $y $x; done | sort -k 1 -n | tail -n2 | while read line; do cat $line; done > VL.top2"
    posix.system(cmd1)
    posix.system(cmd2)
    

def main_MovingConfLevel_only():

    consecutive_num = 3
    denovoProgs = "distiller-full"; contigs_file = ["7-16-4-fullDB.list"]
    contigs_set = ReadContigList(contigs_file)
    template_list = [("fullSeq.fas", "H")]

    for PDB_chain, chainID in template_list:   
        mytemplate, CDR_FR_string = get_TemplateSeq_CDRFRstring_from_fasfile(PDB_chain, chainID)             
        info = """
======================================================================
  Reconstructed Template Assessment by Confidence Level Check
  denovoseq generator = %s
  Contigs mapping 
  ....onto the template = %s
  ....with denovoseq = %s
======================================================================
""" % (denovoProgs, PDB_chain, contigs_file)
        ################################
        MovingConfidenceScore(contigs_set, mytemplate, consecutive_num, denovoProgs, CDR_FR_string, PDB_chain, chainID, info)


def templateRefinement():
    consecutive_num = 3
    denovoProgs = "distiller2"
    contigs_file = ["../7-16-4_all_tags.list"]
    contigs_set = ReadContigList(contigs_file)
    template_list = ["v2.fas", "H"]
    ##############
    for seed in ["SLTC"]:
        mytemplate = ''.join([line.strip() for line in open(template_list[0], 'r').readlines() if not re.search("^>", line)])
        s = difflib.SequenceMatcher(None, mytemplate, seed)
        t = s.get_matching_blocks()
        aindex = t[0][0]; bindex = t[0][1]; overlap = t[0][2] 
        full_seed_str = ' '*(aindex) + seed
                                                                                                                                 
        print mytemplate
        print full_seed_str
        str_all = []
        for count, contig in enumerate(contigs_set):
            if re.search(I2L_K2Q_str(seed), contig):
                s = difflib.SequenceMatcher(None, I2L_K2Q_str(full_seed_str), contig)
                t = s.get_matching_blocks()
                aindex = t[0][0]; bindex = t[0][1]

                string = '-'*(aindex - bindex) + contig
                str_all.append(string)

        str_all.sort(key=lambda line:len(line), reverse=True)
        for _ in str_all:
            print _



def ConfLevel_Chk(consecutive_num, denovoProgs, contigs_file, template_list, recalc):
   
    contigs_set = ReadContigList(contigs_file)
    PDB_chain = template_list[0]
    chainID = template_list[1]
    mytemplate, CDR_FR_string = get_TemplateSeq_CDRFRstring_from_fasfile(PDB_chain, chainID)             
    info = """
======================================================================
  Reconstructed Template Assessment by Confidence Level Check
  Denovoseq generator = %s
  Denovo tags mapping 
  ....onto the template = %s
  ....with denovo tags = %s
======================================================================
""" % (denovoProgs, PDB_chain, contigs_file)
    ################################
    if recalc == 1:
        ConfLevel_filename = MovingConfidenceScore(contigs_set, mytemplate, consecutive_num, denovoProgs, CDR_FR_string, PDB_chain, chainID, info)
    else:
        ConfLevel_filename = denovoProgs + "_" + PDB_chain.replace(".fas", "") + "_" + "EvolvingTemplate.out"

    cur_lines = open(ConfLevel_filename, 'r').readlines()
    current_confidence = ''.join(cur_lines)
    return current_confidence


def denovo_query_search(template_list, contigs_set):
    denovoseg = raw_input("""======================================================================
To search denovo tags to fill the holes, 
Enter the query seq segment 
(usually segment on the high conf level immediately before the low conf level)
======================================================================
-> """)

    seed = "%s" % denovoseg
    mytemplate = ''.join([line.strip() for line in open(template_list[0], 'r').readlines() if not re.search("^>", line)])
    s = difflib.SequenceMatcher(None, mytemplate, seed)
    t = s.get_matching_blocks()
    aindex = t[0][0]; bindex = t[0][1]; overlap = t[0][2] 
    full_seed_str = ' '*(aindex) + seed
                                                                                                                             
    print mytemplate
    print full_seed_str
    str_all = []
    for count, contig in enumerate(contigs_set):
        if re.search(I2L_K2Q_str(seed), contig):
            s = difflib.SequenceMatcher(None, I2L_K2Q_str(full_seed_str), contig)
            t = s.get_matching_blocks()
            aindex = t[0][0]; bindex = t[0][1]

            string = '-'*(aindex - bindex) + contig
            str_all.append(string)

    str_all.sort(key=lambda line:len(line))
    for _ in str_all:
        print _


def MovingConfLevel_TemplateRefinement(prefix, Vchain, start_version, first_recalc):

    #### INPUT #################################### 
    consecutive_num = 3
    denovoProgs = "distiller" 
    contigs_file = ["../../T2/denovoTags/T2_denovo_tags.list"]
    #contigs_file = ["../mascotQA/T2_QA1.uniq"]

    contigs_set = ReadContigList(contigs_file)
    if Vchain == "H":
        template_list = [prefix + "_VH_v%d.fas" % start_version, "H"]
    elif Vchain == "L":
        template_list = [prefix + "_VL_v%d.fas" % start_version, "L"]

    #### CONF LEVEL ############################### 
    cur_confidence = ConfLevel_Chk(consecutive_num, denovoProgs, contigs_file, template_list, recalc=first_recalc)
    print cur_confidence

    #### DENOVO TAG SEARCH ######################## 
    denovo_query_search(template_list, contigs_set)
    
    #### TEMPLATE VERSION CONTROL #################
    cur_version = start_version

    #### ITERATIVE REFINE UNTIL HIT 0 ############# 
    while True: 
        denovoOpt_ = raw_input("""======================================================================
Select...
1. Continue with new seq segment to search denovo tags 
2. Update a template sequence followed by conf level chk
3. Check current template's conf level
4. Check previous template's conf level
quit
======================================================================
-> """)
        denovoOpt = denovoOpt_.strip()
        if denovoOpt == 'quit':
            break 
        else:
            denovoOpt = int(denovoOpt)
            if denovoOpt == 1: # new denovo seg search                                    
                denovo_query_search(template_list, contigs_set)
            elif denovoOpt == 2:
                newseq = raw_input("""enter new seq :
""")
                new_version = cur_version + 1
                ############################
                template_list[0] = template_list[0].replace("%d.fas" % cur_version, "%d.fas" % new_version)
                with open(template_list[0], 'w') as f:
                    print >> f, ">%s" % template_list[0].split(".fas")[0]
                    print >> f, newseq.strip()
                print template_list
                new_confidence = ConfLevel_Chk(consecutive_num, denovoProgs, contigs_file, template_list, recalc=1)
                print new_confidence
                # update conf level : old <=== cur_conf;  cur <=== new_conf
                old_confidence = cur_confidence
                cur_confidence = new_confidence
                # template version control 
                cur_version = new_version
            elif denovoOpt == 3:
                print cur_confidence
                denovo_query_search(template_list, contigs_set)
            elif denovoOpt == 4:
                print old_confidence
                denovo_query_search(template_list, contigs_set)
            else:
                continue
        

if __name__ == "__main__":

    #main_Whole_Workflow_KABATpdbaa()
    #main_Whole_Workflow_NCBIgermline()
    MovingConfLevel_TemplateRefinement(prefix = "NCBI1333982", Vchain="H", start_version=1, first_recalc=1)
    #MovingConfLevel_TemplateRefinement(prefix = "2ZPK", Vchain="L", start_version=10, first_recalc=1)
    #MovingConfLevel_TemplateRefinement(prefix = "1MFB", Vchain="L", start_version=7, first_recalc=0)
    #MovingConfLevel_TemplateRefinement(Vchain="H", start_version=9, first_recalc=1)
    #main_MovingConfLevel_only()
