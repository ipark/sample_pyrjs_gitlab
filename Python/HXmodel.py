import os, sys, posix, re
import numpy as np
import glob
import sys
import math
from  itertools import *
from collections import defaultdict, OrderedDict

cur_dir = os.getcwd()
prefix = cur_dir.split("/")[-3]
constR = 1.98/1000. #kcal/mol*K

def drawProgressBar(percent, barLen = 20):
    sys.stdout.write("\r")
    progress = ""
    for i in range(barLen):
        if i < int(barLen * percent):
            progress += "="
        else:
            progress += " "
    sys.stdout.write("[ %s ] %.2f%%" % (progress, percent * 100))
    sys.stdout.flush()

###################################################################


"""
Ref: 
@article{baiprimary1993,
  title={Primary structure effects on peptide group hydrogen exchange},
  author={Yawen Bai and John S. Milne and Leland Mayne and S. Walter Englander},
  journal={Proteins: Structure, Function, and Bioinformatics},
  volume={17},
  number={1},
  pages={75-86},
  year={1993}, 
}
"""
AA_3to1 = {'GLY':'G', 'ALA':'A', 'VAL':'V', 'LEU':'L', 'ILE':'I', 
           'MET':'M', 'PHE':'F', 'TRP':'W', 'PRO':'P', 'SER':'S', 
           'THR':'T', 'CYS':'C', 'TYR':'Y', 'ASN':'N', 'GLN':'Q', 
           'ASP':'D', 'GLU':'E', 'LYS':'K', 'ARG':'R', 'HIS':'H',
           'NTR':'X', 'CTR':'Z',
           'HIE':'H', 'HID':'H', 'CYX':'C',
           'Y1P':'Y', 'T1P':'T',
           'HIC':'H', 'HIF':'H', 'HIG':'H', 'HIX':'H',
           'ACZ':'D',
           'AS4':'D',
           'GL4':'E',
           'HIP':'H',}
AA_1to3 = {'G':'GLY', 'A':'ALA', 'V':'VAL', 'L':'LEU', 'I':'ILE', 
           'M':'MET', 'F':'PHE', 'W':'TRP', 'P':'PRO', 'S':'SER', 
           'T':'THR', 'C':'CYS', 'Y':'TYR', 'N':'ASN', 'Q':'GLN', 
           'D':'ASP', 'E':'GLU', 'K':'LYS', 'R':'ARG', 'H':'HIS', 
           'X':'NTR', 'Z':'CTR'}


###################################################################
def sequence_kint(pD_corr, myTempC, timeUnit, PDBfile, offset):

    PRO_list = []
    myTemp = float(myTempC) + 273.0
    pD_corr = float(pD_corr)

    mykeys_resname = {}
    #seq = seq.replace("\n", '')                                              
    ########################################################
    # from PDBfile, get sequence using AA_3to1
    with open(PDBfile, 'r') as f:
        for _ in f:
            m = re.match("^ATOM\s+\d+\s+CA\s+([A-Z][A-Z,0-9][A-Z]).+?(\d+) ", _)
            if m and not re.search("HEM", _):
                #if m.group != "PRO":
                #ATOM   1499  CA  SER A 100               
                #0123456789012345678901234567890123456789
                resname = m.group(1)
                resnum = int(m.group(2))
                mykeys_resname[resnum] = resname
                #resname_seq.append(AA_3to1[resname])
                                                                             
    mykeys = mykeys_resname.keys()
    mykeys.sort()
    resname_seq = []


    for r in mykeys:
        r = int(r)
        resname_seq.append(AA_3to1[mykeys_resname[r]])
    seq = ''.join(resname_seq)
    #print seq
    ########################################################

    #print mykeys_resname


    if re.search("sec", timeUnit):
        timeUnitFactor = 1/60.
    else:
        timeUnitFactor = 1.0

    Calc_condition = """
/////////////////////////////////////////////////////////
Temperature = %s K
pD_corr = %s 
Time Unit = %s
Sequence = %s
/////////////////////////////////////////////////////////
""" % (myTemp, pD_corr, timeUnit, seq)

    print Calc_condition
    with open("seq.1R", 'w') as f:
        print >> f, seq


    """
    Table II from Bai et al. (1993)
     log kex(X) - log kex(Ala)
    """
    log_AB_LR_dic = {
    #residue: [AL,     AR,     BL,     BR  ],
    'ALA': [0.00,   0.00,   0.00,   0.00],
    'ARG': [-0.59,  -0.32,  0.08,   0.22],
    'ASN': [-0.58,  -0.13,  0.49,   0.32],
    'ASPd': [0.9,    0.58,   -0.30,  -0.18], #D(COO-)
    'ASPp': [-0.9,   -0.12,   0.69,   0.6],  #D(COOH)
    'CYS': [-0.54,  -0.46,  0.62,   0.55],
    'GLY': [-0.22,  0.22,   0.27,   0.17],
    'GLN': [-0.47,  -0.27,  0.06,   0.20],
    'GLUd': [-0.9,   0.31,   -0.51,  -0.15], #E(COO-)
    'GLUp': [-0.6,  -0.27,    0.24,   0.39], #E(COOH)
    'HISd': ['NA',  'NA',    -1.0,   0.14],
    'HISp': [-0.8,  -0.51,   0.8,    0.83],
    'ILE': [-0.91,  -0.59,  -0.73,  -0.23],
    'ILE': [-0.91,  -0.59,  -0.73,  -0.23],
    'LEU': [-0.57,  -0.13,  -0.58,  -0.21],
    'LYS': [-0.56,  -0.29,  -0.04,  0.12],
    'MET': [-0.64,  -0.28,  -0.01,  0.11],
    'PHE': [-0.52,  -0.43,  -0.24,  0.06],
    'PRO': ['NA',   -0.19,  'NA',   -0.24], #P-trans
    #'PROc': ['NA',   -0.85,  'NA',    0.60],  #P-cis
    'SER': [-0.44,  -0.39,  0.37,   0.30],
    'THR': [-0.79,  -0.47,  -0.07,  0.20],
    'TRP': [-0.40,  -0.44,  -0.41,  -0.11],
    'TYR': [-0.41,  -0.37,  -0.27,  0.05],
    'VAL': [-0.74,  -0.30,  -0.70,  -0.14],
    'NTR':  ['NA',    -1.32,  'NA',   1.62],
    'CTRd':  [0.96,   'NA',   -1.8,  'NA'],
    'CTRp':  [0.05,   'NA',   'NA',  'NA']
    }

    # Bai et al. 1993
    #   protonation state of Asp:D(pka=4.08), Glu:E(pka=4.53), His:H(pka=7.02), Cter:Z(assuming pka=7)
    # SPHERE
    #    HISpka    ASPpka    GLUpka    NH3pka   COOHpka
    #   6.750000  4.500000  4.500000  8.020001  3.720000
    #titrable_res_dic = {'ASP':4.08, 'GLU':4.53, 'HIS':7.02, 'CTR':7.0}  # Bai et al. 1993
    titrable_res_dic = {'ASP':4.50, 'GLU':4.50, 'HIS':6.75, 'CTR':3.720} #, 'NTR':8.02} # SPHERE

    # activation energies (kcal/mol)
    #Ea_A = 14.0; Ea_B = 17.0; Ea_W = 19.0  # Bai 1993
    Ea_A = 15.0; Ea_B = 2.6; Ea_W = 13.0    # SPHERE

    """
    Table I from Bai et al. (1993)
    """
    # standard reference rate constants at 293K
    ###########################################
    ## at high salt condition (0.5 KCl) oligo-peptide (from TableIII of Bai 1993)
    """
    kA_ref = (timeUnitFactor)*10**1.56  #1/(M*min)
    kB_ref = (timeUnitFactor)*10**10.20 #1/(M*min)
    kW_ref = (timeUnitFactor)*10**-2.3  #1/min    
    """
    ## at high salt condition (0.5 KCl) poly-peptide (from TableIII of Bai 1993)
    """
    kA_ref = (timeUnitFactor)*10**1.19  #1/(M*min)
    kB_ref = (timeUnitFactor)*10**9.900 #1/(M*min)
    kW_ref = (timeUnitFactor)*10**-2.5  #1/min    
    """
    # at normal lower salt condition oligo-peptide (from TableIII of Bai 1993)
    #kA_ref = (timeUnitFactor)*10.**2.04   #1/(M*min)
    #kB_ref = (timeUnitFactor)*10.**10.36  #1/(M*min)
    #kW_ref = (timeUnitFactor)*10.**-1.5   #1/min
    #logkA_ref = math.log10((timeUnitFactor)*10**2.04)   #1/(M*min)
    #logkB_ref = math.log10((timeUnitFactor)*10**10.36)  #1/(M*min)
    #logkW_ref = math.log10((timeUnitFactor)*10**-1.5)   #1/min
    logkA_ref = math.log10(10**2.04)   #1/(M*min)
    logkB_ref = math.log10(10**10.36)  #1/(M*min)
    logkW_ref = math.log10(10**-1.5)   #1/min
    # at normal lower salt condition poly-peptide (from TableIII of Bai 1993)
    """
    kA_ref = (timeUnitFactor)*10**1.62  #1/(M*min)
    kB_ref = (timeUnitFactor)*10**10.05 #1/(M*min)
    kW_ref = (timeUnitFactor)*10**-1.5  #1/min
    """
    ###########################################

    # pK(D, 20C) = 15.65
    #pkD = 15.65
    pkD = 15.05 # SPHERE; Connelly 1993
    ##############################
    #pD_corr =  7.4 # pD_read + 0.4
    pOD_corr = pkD - pD_corr
    ##############################
    #Dcation = 10**-(pD_corr)  # pD = -log10[D+]
    #ODanion = 10**-(pOD_corr)


    """
    seq_NH = 'x'.join(seq)
    for k, v in enumerate(seq_NH):
        logk_acid = 0; logk_base =0; logk_wat = 0; logk_int = 0
        # YxRxAxPxExIxM  IxV
        #  |              |
        # R L            R L
        if v == 'x':
            res_R = AA_1to3[seq_NH[k - 1]]
            res_L = AA_1to3[seq_NH[k + 1]]
            
            # default PRO = trans
            if res_R == 'PRO':
                res_R = 'PROt'            
            if res_L == 'PRO':
                res_L = 'PROt'
    """
    mykeys = mykeys_resname.keys()
    mykeys.sort()
    resnum_min = mykeys[0]
    resnum_max = mykeys[-1]

    kint_resnum_dic = {}
    for r in mykeys:
        # YRAPEIM 
        #  | |  |   
        # RLRL RL

        if r > resnum_min and r <= resnum_max:

            #res_R = AA_1to3[seq[r - 1]]
            #res_L = AA_1to3[seq[r]]
            if mykeys_resname.has_key(r - 1):
                res_R = AA_1to3[AA_3to1[mykeys_resname[r - 1]]]
            if mykeys_resname.has_key(r):
                res_L = AA_1to3[AA_3to1[mykeys_resname[r]]]

            #print res_R, res_L
            # protonation state of titrable residues
            special_res = titrable_res_dic.keys()
            for s_res in special_res:
                #print s_res
                if res_R == s_res:
                    if pD_corr <= titrable_res_dic[s_res]:
                        res_R = res_R + 'p'
                    else:
                        res_R = res_R + 'd'
                if res_L == s_res:
                    if pD_corr <= titrable_res_dic[s_res]:
                        res_L = res_L + 'p'
                    else:
                        res_L = res_L + 'd'

                
            if log_AB_LR_dic[res_L][0] == 'NA':
                #A_L = 1.0
                logA_L = 0.0
            else:
                #A_L = 10**(log_AB_LR_dic[res_L][0])                                                                                  
                logA_L = log_AB_LR_dic[res_L][0]
            #
            if log_AB_LR_dic[res_R][1] == 'NA':
                #A_R = 1.0
                logA_R = 0.0
            else:
                #A_R = 10**(log_AB_LR_dic[res_R][1])
                logA_R = log_AB_LR_dic[res_R][1]
            #
            if log_AB_LR_dic[res_L][2] == 'NA':
                #B_L = 1.0
                logB_L = 0.0
            else:
                #B_L = 10**(log_AB_LR_dic[res_L][2])
                logB_L = log_AB_LR_dic[res_L][2]
            # 
            if log_AB_LR_dic[res_R][3] == 'NA':
                #B_R = 1.0
                logB_R = 0.0
            else:
                #B_R = 10**(log_AB_LR_dic[res_R][3])
                logB_R = log_AB_LR_dic[res_R][3]
            
            """
            Eqn(2) from Bai et al. (1993)
            """
            #kint = krc_293K * math.exp(-Ea*(1/float(myTemp)-1/293.0)/constR)
            TempFactor = (1.0/myTemp - 1.0/293.0)/constR
            #k_acid = (kA_ref * A_L * A_R * Dcation)  * math.exp(-Ea_A*TempFactor)
            #k_base = (kB_ref * B_L * B_R * ODanion)  * math.exp(-Ea_B*TempFactor)
            #k_wat =  (kW_ref * B_L * B_R)            * math.exp(-Ea_W*TempFactor)
            #k_int = (k_acid + k_base + k_wat)
            logk_acid = (logkA_ref + logA_L + logA_R - pD_corr) 
            logk_base = (logkB_ref + logB_L + logB_R - pOD_corr) 
            logk_wat  = (logkW_ref + logB_L + logB_R)
            #
            k_acid = (10**(logk_acid)) * (math.exp(-Ea_A*TempFactor))
            k_base = (10**(logk_base)) * (math.exp(-Ea_B*TempFactor))
            k_wat  = (10**(logk_wat))  * (math.exp(-Ea_W*TempFactor))
            
            k_int = (k_acid + k_base + k_wat) * timeUnitFactor

            """
            if k == 1:
                print AA_3to1[res_R[:3]]
            if res_L == 'PROt' or res_L == 'PROc':
                print  AA_3to1[res_L[:3]]
            #elif res_L == 'CTRp' or res_L == 'CTRd':
            #    print  res_L
            """
            #print  res_L, '{:5.3e}'.format(k_acid), '{:5.3e}'.format(k_base), '{:5.3e}'.format(k_wat), '{:5.3e}'.format(k_int), "1/min"
            #print  AA_3to1[res_L[:3]], '{:5.3f}'.format(k_int), "1/min"
            if res_L == 'PRO':
                #print AA_3to1[res_R[:3]]+'-NH-'+ AA_3to1[res_L[:3]]
                #continue
                PRO_list.append(int(r))
            else:
                #print AA_3to1[res_R[:3]]+'-NH-'+ AA_3to1[res_L[:3]], '{:3.2e}'.format(k_int), "1/"+str(timeUnit)
                kint_resnum_dic[r] = k_int

    #kint_list = [kint_resnum_dic[r] for r in kint_resnum_dic.keys()]
    #kint_max = max(kint_list)
    #print kint_max

    return mykeys_resname, kint_resnum_dic, PRO_list

###################################################################


def Hbond_wat_prot_MD_logistic(PDBfile, mykeys_resname, kint_resnum_dic, base, timeUnit, PRO_list, scienceFormat):


    """
    mykeys_resname = {}
    with open(PDBfile, 'r') as f:
        for _ in f:
            m = re.match("^ATOM\s+\d+\s+CA\s+([A-Z][A-Z][A-Z]).+?(\d+) ", _)
            if m:
                #if m.group != "PRO":
                #ATOM   1499  CA  SER A 100               
                #0123456789012345678901234567890123456789
                resname = m.group(1)
                resnum = int(m.group(2))
                mykeys_resname[resnum] = resname
    """

    mykeys = mykeys_resname.keys()
    mykeys.sort()

    RootHbondALL = '/'.join(PDBfile.split('/')[:len(PDBfile.split('/'))-1])
    name = os.path.basename(PDBfile).replace("_tleap.pdb", "")

    Hbond_list = [line for line in glob.glob(RootHbondALL + "/HbondALL/hbond.*[0-9]")]
    Hbond_list.sort(key = lambda line:int(line.split('.')[-1]))
    snapshot_list = [line.split('.')[-1].lstrip('0') for line in Hbond_list]
    total_snap = len(Hbond_list)

    """
    Finding intermodel H-bonds
    Finding intramodel H-bonds
    Constraints relaxed by 0.4 angstroms and 20 degrees
    Models used:
    	#0 pr.crd
    
    H-bonds (donor, acceptor, hydrogen, D..A dist, D-H..A dist):
    WAT 98888.water O   ASP 401 OD2         WAT 98888.water H1   2.604  1.725
    WAT 99040.water O   SER 243 O           WAT 99040.water H1   2.779  1.825
    WAT 99047.water O   ASN 1291 O          WAT 99047.water H1   2.885  2.013

    ARG 18 NE           WAT 42617.water O   ARG 18 HE            2.719  1.722
    ARG 18 NH1          WAT 1535.water O    ARG 18 HH11          2.949  1.984
    ARG 18 NH1          WAT 26900.water O   ARG 18 HH12          3.023  2.145
    """

    mykeys = mykeys_resname.keys()
    mykeys.sort()
    
    Hbond_dic_NH_wat = defaultdict(defaultdict)
    Hbond_dic_CO_wat = defaultdict(defaultdict)
    Hbond_dic_NH_CO =  defaultdict(defaultdict) 
    Hbond_dic_NH_SC =  defaultdict(defaultdict)
    Hbond_dic_CO_SC =  defaultdict(defaultdict)

    count = 0
    for hbond_info in Hbond_list:
        count += 1
        percent = count*1.0/total_snap 
        drawProgressBar(percent)
        snap_no = hbond_info.split('.')[-1].lstrip('0')
        with open(hbond_info, 'r') as f:                                                                                                  
            for _ in f:
                            #WAT 99047.water O   ASN 1291 O          WAT 99047.water H1   2.885  2.013
                            #ARG 18 NE           WAT 42617.water O   ARG 18 HE            2.719  1.722
                m = re.match("^(\S+) (\S+) (\S+)\s+(\S+) (\S+) (\S+)\s+(\S+) (\S+) (\S+) ", _)
                #                1      2     3      4     5      6       7    8     9 
                if m and not re.search("HEM", _):
                    Dres = m.group(1); Datom = m.group(3)
                    Ares = m.group(4); Aatom = m.group(6)
                    Hres = m.group(7); Hatom = m.group(9)
                    Dresnum = int(m.group(2).replace(".water", ""))
                    Aresnum = int(m.group(5).replace(".water", ""))
                    Hresnum = int(m.group(8).replace(".water", ""))

                    #print Dresnum, Aresnum, Hresnum
                    for r in mykeys:
                        #############
                        # water-amide
                        #############
                        #ARG 18 N            WAT 42617.water O   ARG 18 H             2.719  1.722  
                        #       Datom        Ares                Hresnum Hatom
                        #--------------------------------------------------------------------------
                        if Ares == "WAT": # NH[D,H]...O-HH[A]
                            if Hresnum == r and Hatom == "H" and Datom == "N": 
                                Hbond_dic_NH_wat[r][snap_no] = 1
                        #WAT 37753.water O   THR 752 O           WAT 37753.water H1   2.709  1.762  
                        # Dres              Aresnum  Aatom                     Hatom
                        #--------------------------------------------------------------------------
                        elif Dres == "WAT":  #O[A]...H1-OH[D,H] 
                            if Aresnum == r and Aatom == "O" and Hatom == "H1":
                                Hbond_dic_CO_wat[r][snap_no] = 1
                        #############
                        # amide-amide
                        #############
                        # H-bond donor : NH
                        elif Hresnum == r and Hatom == "H" and Datom == "N": 
                            if Aatom == "O":
                                Hbond_dic_NH_CO[r][snap_no] = 1
                            else:
                                Hbond_dic_NH_SC[r][snap_no] = 1
                        # amide-amide
                        # H-bond acceptor : backbone carbonyl O
                        elif Aresnum == r and Aatom == "O" and Datom != "N": 
                            Hbond_dic_CO_SC[r][snap_no] = 1


    kint_list = [kint_resnum_dic[r] for r in kint_resnum_dic.keys()]
    kint_avg = sum(kint_list)/len(kint_list)
    kint_max = max(kint_list)
    #print kint_max


    mykeys = mykeys_resname.keys() 
    mykeys.sort()

    if scienceFormat == 1:

        
        d = open("%sC_pD%s_base%s_timeUnit%s.wat.logistic.HBstat.%s" % (myTempC, pD_corr, str('{:1.0e}'.format(base)), timeUnit.replace('"', ''), name), 'w')
        print >> d, '{:10s}'.format("ResNum"), '{:10s}'.format("Residue"),\
'{:10s}'.format("frac_NH_CO"), \
'{:10s}'.format("frac_NH_wat"), \
'{:10s}'.format("frac_non_NHbond"), \
'{:10s}'.format("frac_NH_SC"), \
'{:10s}'.format("frac_CO_SC"), \
'{:10s}'.format("frac_CO_wat"),\
'{:10s}'.format("frac_none_Hbond")
        
        with open("%sC_pD%s_base%s_timeUnit%s.wat.logistic.%s" % (myTempC, pD_corr, str('{:1.0e}'.format(base)), timeUnit.replace('"', ''), name), 'w') as c:
            #print >> c, "ResNum\tResidue\tTotalSnap\tkint\tPFj\tNH:O=C\tNH:sc\tC=O:sc\tNH:O-HH\tC=O:H1-OH\tFold\tUnfold\tTot_Hbond"
            print >> c, '{:10s}'.format("ResNum"), '{:10s}'.format("Residue"), '{:10s}'.format("kintj"), \
'{:10s}'.format("logPFj_Hopt0"), '{:10s}'.format("kexj_Hopt0"), \
'{:10s}'.format("logPFj_Hopt1"), '{:10s}'.format("kexj_Hopt1"), \
'{:10s}'.format("logPFj_Hopt2"), '{:10s}'.format("kexj_Hopt2"), \
'{:10s}'.format("logPFj_Hopt3"), '{:10s}'.format("kexj_Hopt3"), \
'{:10s}'.format("HB0_portion"), \
'{:10s}'.format("HB1_portion"), \
'{:10s}'.format("HB2_portion"), \
'{:10s}'.format("HB3_portion")

            for r in mykeys:                                                                                                               
                renum_m = int(r) + int(offset)
                Hbond_NH_wat = 0 
                Hbond_CO_wat = 0
                Hbond_NH_CO = 0
                Hbond_NH_SC = 0
                Hbond_CO_SC = 0
                PFj_Hopt0 = 0.0
                PFj_Hopt1 = 0.0
                PFj_Hopt2 = 0.0
                PFj_Hopt3 = 0.0
                PFpower_Hopt0 = 0.0
                PFpower_Hopt1 = 0.0
                PFpower_Hopt2 = 0.0
                PFpower_Hopt3 = 0.0
                kexj_Hopt0 = 0.0 
                kexj_Hopt1 = 0.0 
                kexj_Hopt2 = 0.0 
                kexj_Hopt3 = 0.0 

                frac_NH_CO =  0.0
                frac_NH_wat = 0.0
                frac_non_NHbond = 0.0
                frac_NH_SC = 0.0
                frac_CO_SC = 0.0
                frac_CO_wat = 0.0
                frac_none = 0.0


                if kint_resnum_dic.has_key(r):
                    #kint_j = '{:3.2f}'.format(kint_resnum_dic[r])
                    kint_j = kint_resnum_dic[r]
                else:
                    kint_j = ' '
                for i in snapshot_list:
                    if Hbond_dic_NH_wat[r].has_key(i): 
                        Hbond_NH_wat += 1 
                    elif Hbond_dic_CO_wat[r].has_key(i): 
                        Hbond_CO_wat += 1 
                    elif Hbond_dic_NH_CO[r].has_key(i): 
                        Hbond_NH_CO += 1 
                    elif Hbond_dic_NH_SC[r].has_key(i): 
                        Hbond_NH_SC += 1 
                    elif Hbond_dic_CO_SC[r].has_key(i): 
                        Hbond_CO_SC += 1 
                if kint_j != ' ':
                    kint_j = float(kint_j)
                    #
                    #if Hbond_opt == 0: # only NH_CO 
                    #
                    PFpower_Hopt0 = (1.0*(Hbond_NH_CO)- 0.0*(Hbond_NH_wat))/total_snap
                    ###PFjval_Hopt0 = base**PFpower_Hopt0
                    PFjval_Hopt0 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt0) )
                    kexj_Hopt0 = kint_j/PFjval_Hopt0
                    #
                    #if Hbond_opt == 1: # default
                    #
                    PFpower_Hopt1 = (1.0*(Hbond_NH_CO)- 1.0*(Hbond_NH_wat))/total_snap
                    ###PFjval_Hopt1 = base**PFpower_Hopt1
                    PFjval_Hopt1 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt1) )
                    kexj_Hopt1 = kint_j/PFjval_Hopt1
                    #
                    #elif Hbond_opt == 2: # NH only
                    #
                    PFpower_Hopt2 = (1.0*(Hbond_NH_CO + Hbond_NH_SC) - 1.0*(Hbond_NH_wat))/total_snap
                    ###PFjval_Hopt2 = base**PFpower_Hopt2
                    PFjval_Hopt2 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt2) )
                    kexj_Hopt2 = kint_j/PFjval_Hopt2
                    #
                    #elif Hbond_opt == 3: # ALL
                    #
                    PFpower_Hopt3 = (1.0*(Hbond_NH_CO+Hbond_NH_SC+Hbond_CO_SC) - 1.0*(Hbond_NH_wat+Hbond_CO_wat))/total_snap
                    ###PFjval_Hopt3 = base**PFpower_Hopt3
                    PFjval_Hopt3 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt3) )
                    kexj_Hopt3 = kint_j/PFjval_Hopt3
                                                                                                                                           

                    frac_NH_CO =  100.0*(Hbond_NH_CO)/total_snap                                         
                    frac_NH_wat = 100.0*(Hbond_NH_wat)/total_snap
                    frac_non_NHbond = 100.0 - frac_NH_CO - frac_NH_wat
                    frac_NH_SC = 100.0*(Hbond_NH_SC)/total_snap
                    frac_CO_SC = 100.0*(Hbond_CO_SC)/total_snap 
                    frac_CO_wat = 100.0*(Hbond_CO_wat)/total_snap
                    frac_none = 100.0 - frac_NH_CO - frac_NH_wat - frac_NH_SC - frac_CO_SC - frac_CO_wat

                    #
                    if (r + 1 in PRO_list):
                        print >> c, '{:10d}'.format(renum_m),\
'{:10s}'.format(mykeys_resname[r]), \
'{:3.2e}'.format(kint_j),\
'{:3.2f}'.format(math.log10(PFjval_Hopt0)), \
'{:3.2e}'.format(kexj_Hopt0), \
'{:3.2f}'.format(math.log10(PFjval_Hopt1)), \
'{:3.2e}'.format(kexj_Hopt1), \
'{:3.2f}'.format(math.log10(PFjval_Hopt2)), \
'{:3.2e}'.format(kexj_Hopt2), \
'{:3.2f}'.format(math.log10(PFjval_Hopt3)), \
'{:3.2e}'.format(kexj_Hopt3), \
'{:10.5f}'.format(PFpower_Hopt0), \
'{:10.5f}'.format(PFpower_Hopt1), \
'{:10.5f}'.format(PFpower_Hopt2), \
'{:10.5f}'.format(PFpower_Hopt3)
                        print >> c, '{:10s}'.format('       #'+str(renum_m + 1)), '{:10s}'.format('PRO')
                        print >> d, '{:10d}'.format(renum_m), '{:10s}'.format(mykeys_resname[r]), \
'{:10.1f}'.format(frac_NH_CO), \
'{:10.1f}'.format(frac_NH_wat), \
'{:10.1f}'.format(frac_non_NHbond), \
'{:10.1f}'.format(frac_NH_SC), \
'{:10.1f}'.format(frac_CO_SC), \
'{:10.1f}'.format(frac_CO_wat), \
'{:10.1f}'.format(frac_none)
                        print >> d, '{:10s}'.format('       #'+str(renum_m + 1)), '{:10s}'.format('PRO')    
                    else:
                        print >> c, '{:10d}'.format(renum_m),\
'{:10s}'.format(mykeys_resname[r]), \
'{:3.2e}'.format(kint_j),\
'{:3.2f}'.format(math.log10(PFjval_Hopt0)), \
'{:3.2e}'.format(kexj_Hopt0), \
'{:3.2f}'.format(math.log10(PFjval_Hopt1)), \
'{:3.2e}'.format(kexj_Hopt1), \
'{:3.2f}'.format(math.log10(PFjval_Hopt2)), \
'{:3.2e}'.format(kexj_Hopt2), \
'{:3.2f}'.format(math.log10(PFjval_Hopt3)), \
'{:3.2e}'.format(kexj_Hopt3),\
'{:10.5f}'.format(PFpower_Hopt0), \
'{:10.5f}'.format(PFpower_Hopt1), \
'{:10.5f}'.format(PFpower_Hopt2), \
'{:10.5f}'.format(PFpower_Hopt3)
                        print >> d, '{:10d}'.format(renum_m), '{:10s}'.format(mykeys_resname[r]), \
'{:10.1f}'.format(frac_NH_CO), \
'{:10.1f}'.format(frac_NH_wat), \
'{:10.1f}'.format(frac_non_NHbond), \
'{:10.1f}'.format(frac_NH_SC), \
'{:10.1f}'.format(frac_CO_SC), \
'{:10.1f}'.format(frac_CO_wat), \
'{:10.1f}'.format(frac_none)


#############################################
def Hbond_wat_prot_MD_logistic_HBmore(PDBfile, mykeys_resname, kint_resnum_dic, base, timeUnit, PRO_list, scienceFormat):


    mykeys = mykeys_resname.keys()
    mykeys.sort()

    RootHbondALL = '/'.join(PDBfile.split('/')[:len(PDBfile.split('/'))-1])
    name = os.path.basename(PDBfile).replace("_tleap.pdb", "")

    Hbond_list = [line for line in glob.glob(RootHbondALL + "/HbondALL/hbond.*[0-9]")]
    Hbond_list.sort(key = lambda line:int(line.split('.')[-1]))
    snapshot_list = [line.split('.')[-1].lstrip('0') for line in Hbond_list]
    total_snap = len(Hbond_list)


    mykeys = mykeys_resname.keys()
    mykeys.sort()
    
    Hbond_dic_NH_wat = defaultdict(defaultdict)
    Hbond_dic_CO_wat = defaultdict(defaultdict)
    Hbond_dic_NH_CO =  defaultdict(defaultdict) 
    Hbond_dic_NH_SC =  defaultdict(defaultdict)
    Hbond_dic_CO_SC =  defaultdict(defaultdict)

    count = 0
    for hbond_info in Hbond_list:
        count += 1
        percent = count*1.0/total_snap 
        drawProgressBar(percent)
        snap_no = hbond_info.split('.')[-1].lstrip('0')
        with open(hbond_info, 'r') as f:                                                                                                  
            for _ in f:
                            #WAT 99047.water O   ASN 1291 O          WAT 99047.water H1   2.885  2.013
                            #ARG 18 NE           WAT 42617.water O   ARG 18 HE            2.719  1.722
                m = re.match("^(\S+) (\S+) (\S+)\s+(\S+) (\S+) (\S+)\s+(\S+) (\S+) (\S+) ", _)
                #                1      2     3      4     5      6       7    8     9 
                if m and not re.search("HEM", _):
                    Dres = m.group(1); Datom = m.group(3)
                    Ares = m.group(4); Aatom = m.group(6)
                    Hres = m.group(7); Hatom = m.group(9)
                    Dresnum = int(m.group(2).replace(".water", ""))
                    Aresnum = int(m.group(5).replace(".water", ""))
                    Hresnum = int(m.group(8).replace(".water", ""))

                    #print Dresnum, Aresnum, Hresnum
                    for r in mykeys:
                        #############
                        # water-amide
                        #############
                        #ARG 18 N            WAT 42617.water O   ARG 18 H             2.719  1.722  
                        #       Datom        Ares                Hresnum Hatom
                        #--------------------------------------------------------------------------
                        if Ares == "WAT": # NH[D,H]...O-HH[A]
                            if Hresnum == r and Hatom == "H" and Datom == "N": 
                                Hbond_dic_NH_wat[r][snap_no] = 1
                        #WAT 37753.water O   THR 752 O           WAT 37753.water H1   2.709  1.762  
                        # Dres              Aresnum  Aatom                     Hatom
                        #--------------------------------------------------------------------------
                        elif Dres == "WAT":  #O[A]...H1-OH[D,H] 
                            if Aresnum == r and Aatom == "O" and Hatom == "H1":
                                Hbond_dic_CO_wat[r][snap_no] = 1
                        #############
                        # amide-amide
                        #############
                        # H-bond donor : NH
                        elif Hresnum == r and Hatom == "H" and Datom == "N": 
                            if Aatom == "O":
                                Hbond_dic_NH_CO[r][snap_no] = 1
                            else:
                                Hbond_dic_NH_SC[r][snap_no] = 1
                        # amide-amide
                        # H-bond acceptor : backbone carbonyl O
                        elif Aresnum == r and Aatom == "O" and Datom != "N": 
                            Hbond_dic_CO_SC[r][snap_no] = 1


    kint_list = [kint_resnum_dic[r] for r in kint_resnum_dic.keys()]
    kint_avg = sum(kint_list)/len(kint_list)
    kint_max = max(kint_list)
    #print kint_max


    mykeys = mykeys_resname.keys() 
    mykeys.sort()

    if scienceFormat == 1:

        
        d = open("%sC_pD%s_base%s_timeUnit%s.wat.logistic.HBstat.%s" % (myTempC, pD_corr, str('{:1.0e}'.format(base)), timeUnit.replace('"', ''), name), 'w')
        print >> d, '{:10s}'.format("ResNum"), '{:10s}'.format("Residue"),\
'{:10s}'.format("frac_NH_CO"), \
'{:10s}'.format("frac_NH_wat"), \
'{:10s}'.format("frac_non_NHB"), \
'{:10s}'.format("frac_NH_SC"), \
'{:10s}'.format("frac_CO_SC"), \
'{:10s}'.format("frac_CO_wat"),\
'{:10s}'.format("frac_non_AllHB"), \
'{:10s}'.format("frac_non_HB_count")
        
        with open("%sC_pD%s_base%s_timeUnit%s.wat.logistic.%s" % (myTempC, pD_corr, str('{:1.0e}'.format(base)), timeUnit.replace('"', ''), name), 'w') as c:
            #print >> c, "ResNum\tResidue\tTotalSnap\tkint\tPFj\tNH:O=C\tNH:sc\tC=O:sc\tNH:O-HH\tC=O:H1-OH\tFold\tUnfold\tTot_Hbond"
            print >> c, '{:10s}'.format("ResNum"), '{:10s}'.format("Residue"), '{:10s}'.format("kintj"), \
'{:10s}'.format("logPFj_Hopt0"), '{:10s}'.format("kexj_Hopt0"), \
'{:10s}'.format("logPFj_Hopt1"), '{:10s}'.format("kexj_Hopt1"), \
'{:10s}'.format("logPFj_Hopt2"), '{:10s}'.format("kexj_Hopt2"), \
'{:10s}'.format("logPFj_Hopt3"), '{:10s}'.format("kexj_Hopt3"), \
'{:10s}'.format("logPFj_Hopt4"), '{:10s}'.format("kexj_Hopt4"), '{:10s}'.format("logPFj_Hopt5"), '{:10s}'.format("kexj_Hopt5"),  '{:10s}'.format("logPFj_Hopt6"), '{:10s}'.format("kexj_Hopt6"), \
'{:10s}'.format("HB0_portion"), \
'{:10s}'.format("HB1_portion"), \
'{:10s}'.format("HB2_portion"), \
'{:10s}'.format("HB3_portion"), \
'{:10s}'.format("HB4_portion"), '{:10s}'.format("HB5_portion"), '{:10s}'.format("HB6_portion")

            for r in mykeys:                                                                                                               
                renum_m = int(r) + int(offset)
                Hbond_NH_wat = 0; Hbond_CO_wat = 0
                Hbond_NH_CO = 0; Hbond_NH_SC = 0; Hbond_CO_SC = 0
                non_HB_count = 0

                PFj_Hopt0 = 0.0; PFj_Hopt1 = 0.0; PFj_Hopt2 = 0.0; PFj_Hopt3 = 0.0
                PFj_Hopt4 = 0.0; PFj_Hopt5 = 0.0; PFj_Hopt6 = 0.0

                PFpower_Hopt0 = 0.0; PFpower_Hopt1 = 0.0; PFpower_Hopt2 = 0.0; PFpower_Hopt3 = 0.0
                PFpower_Hopt4 = 0.0; PFpower_Hopt5 = 0.0; PFpower_Hopt6 = 0.0

                kexj_Hopt0 = 0.0; kexj_Hopt1 = 0.0; kexj_Hopt2 = 0.0; kexj_Hopt3 = 0.0 
                kexj_Hopt4 = 0.0; kexj_Hopt5 = 0.0; kexj_Hopt6 = 0.0 

                frac_NH_CO =  0.0
                frac_NH_wat = 0.0
                frac_non_NHB = 0.0
                frac_NH_SC = 0.0
                frac_CO_SC = 0.0
                frac_CO_wat = 0.0
                frac_non_AllHB = 0.0
                frac_non_HB_count = 0.0


                if kint_resnum_dic.has_key(r):
                    #kint_j = '{:3.2f}'.format(kint_resnum_dic[r])
                    kint_j = kint_resnum_dic[r]
                else:
                    kint_j = ' '
                for i in snapshot_list:
                    if Hbond_dic_NH_wat[r].has_key(i): 
                        Hbond_NH_wat += 1 
                    elif Hbond_dic_CO_wat[r].has_key(i): 
                        Hbond_CO_wat += 1 
                    elif Hbond_dic_NH_CO[r].has_key(i): 
                        Hbond_NH_CO += 1 
                    elif Hbond_dic_NH_SC[r].has_key(i): 
                        Hbond_NH_SC += 1 
                    elif Hbond_dic_CO_SC[r].has_key(i): 
                        Hbond_CO_SC += 1 
                    else:
                        non_HB_count += 1
                if kint_j != ' ':
                    kint_j = float(kint_j)
                    #
                    #if Hbond_opt == 0: # only NH_CO 
                    PFpower_Hopt0 = (1.0*(Hbond_NH_CO)- 0.0*(Hbond_NH_wat))/total_snap
                    PFjval_Hopt0 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt0) )
                    kexj_Hopt0 = kint_j/PFjval_Hopt0
                    #
                    #if Hbond_opt == 1: # default
                    PFpower_Hopt1 = (1.0*(Hbond_NH_CO)- 1.0*(Hbond_NH_wat))/total_snap
                    PFjval_Hopt1 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt1) )
                    kexj_Hopt1 = kint_j/PFjval_Hopt1
                    #
                    #elif Hbond_opt == 2: # NH only
                    PFpower_Hopt2 = (1.0*(Hbond_NH_CO + Hbond_NH_SC) - 1.0*(Hbond_NH_wat))/total_snap
                    PFjval_Hopt2 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt2) )
                    kexj_Hopt2 = kint_j/PFjval_Hopt2
                    #
                    #elif Hbond_opt == 3: # ALL
                    PFpower_Hopt3 = (1.0*(Hbond_NH_CO+Hbond_NH_SC+Hbond_CO_SC) - 1.0*(Hbond_NH_wat+Hbond_CO_wat))/total_snap
                    PFjval_Hopt3 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt3) )
                    kexj_Hopt3 = kint_j/PFjval_Hopt3
                    #
                    #elif Hbond_opt == 4: # 1/2 nonHB->PP_HB ; 1/2 nonHB->PW_HB
                    PFpower_Hopt4 = (1.0*(Hbond_NH_CO+Hbond_NH_SC+Hbond_CO_SC+non_HB_count/2) - 1.0*(Hbond_NH_wat+Hbond_CO_wat+non_HB_count/2))/total_snap
                    PFjval_Hopt4 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt4) )
                    kexj_Hopt4 = kint_j/PFjval_Hopt4
                    #
                    #elif Hbond_opt == 5: # nonHB->PP_HB
                    PFpower_Hopt5 = (1.0*(Hbond_NH_CO+Hbond_NH_SC+Hbond_CO_SC+non_HB_count) - 1.0*(Hbond_NH_wat+Hbond_CO_wat))/total_snap
                    PFjval_Hopt5 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt5) )
                    kexj_Hopt5 = kint_j/PFjval_Hopt5
                    #
                    #elif Hbond_opt == 6: # nonHB->PW_HB
                    PFpower_Hopt6 = (1.0*(Hbond_NH_CO+Hbond_NH_SC+Hbond_CO_SC) - 1.0*(Hbond_NH_wat+Hbond_CO_wat+non_HB_count))/total_snap
                    PFjval_Hopt6 = base / ( 1 + (math.sqrt(base)) * ((1.0/math.sqrt(base))**PFpower_Hopt6) )
                    kexj_Hopt6 = kint_j/PFjval_Hopt6
                                                                                                                                           

                    frac_NH_CO =  100.0*(Hbond_NH_CO)/total_snap                                         
                    frac_NH_wat = 100.0*(Hbond_NH_wat)/total_snap
                    frac_non_NHB = 100.0 - frac_NH_CO - frac_NH_wat
                    frac_NH_SC = 100.0*(Hbond_NH_SC)/total_snap
                    frac_CO_SC = 100.0*(Hbond_CO_SC)/total_snap 
                    frac_CO_wat = 100.0*(Hbond_CO_wat)/total_snap
                    frac_non_AllHB = 100.0 - frac_NH_CO - frac_NH_wat - frac_NH_SC - frac_CO_SC - frac_CO_wat
                    frac_non_HB_count = 100.0*non_HB_count/total_snap

                    #
                    if (r + 1 in PRO_list):
                        print >> c, '{:10d}'.format(renum_m),\
'{:10s}'.format(mykeys_resname[r]), \
'{:3.2e}'.format(kint_j),\
'{:3.2f}'.format(math.log10(PFjval_Hopt0)), \
'{:3.2e}'.format(kexj_Hopt0), \
'{:3.2f}'.format(math.log10(PFjval_Hopt1)), \
'{:3.2e}'.format(kexj_Hopt1), \
'{:3.2f}'.format(math.log10(PFjval_Hopt2)), \
'{:3.2e}'.format(kexj_Hopt2), \
'{:3.2f}'.format(math.log10(PFjval_Hopt3)), \
'{:3.2e}'.format(kexj_Hopt3), \
'{:3.2f}'.format(math.log10(PFjval_Hopt4)), \
'{:3.2e}'.format(kexj_Hopt4), \
'{:3.2f}'.format(math.log10(PFjval_Hopt5)), \
'{:3.2e}'.format(kexj_Hopt5), \
'{:3.2f}'.format(math.log10(PFjval_Hopt6)), \
'{:3.2e}'.format(kexj_Hopt6), \
'{:10.5f}'.format(PFpower_Hopt0), \
'{:10.5f}'.format(PFpower_Hopt1), \
'{:10.5f}'.format(PFpower_Hopt2), \
'{:10.5f}'.format(PFpower_Hopt3), \
'{:10.5f}'.format(PFpower_Hopt4), \
'{:10.5f}'.format(PFpower_Hopt5), \
'{:10.5f}'.format(PFpower_Hopt6)
                        print >> c, '{:10s}'.format('       #'+str(renum_m + 1)), '{:10s}'.format('PRO')
                        print >> d, '{:10d}'.format(renum_m), '{:10s}'.format(mykeys_resname[r]), \
'{:10.1f}'.format(frac_NH_CO), \
'{:10.1f}'.format(frac_NH_wat), \
'{:10.1f}'.format(frac_non_NHB), \
'{:10.1f}'.format(frac_NH_SC), \
'{:10.1f}'.format(frac_CO_SC), \
'{:10.1f}'.format(frac_CO_wat), \
'{:10.1f}'.format(frac_non_AllHB), \
'{:10.1f}'.format(frac_non_HB_count)
                        print >> d, '{:10s}'.format('       #'+str(renum_m + 1)), '{:10s}'.format('PRO')    
                    else:
                        print >> c, '{:10d}'.format(renum_m),\
'{:10s}'.format(mykeys_resname[r]), \
'{:3.2e}'.format(kint_j),\
'{:3.2f}'.format(math.log10(PFjval_Hopt0)), \
'{:3.2e}'.format(kexj_Hopt0), \
'{:3.2f}'.format(math.log10(PFjval_Hopt1)), \
'{:3.2e}'.format(kexj_Hopt1), \
'{:3.2f}'.format(math.log10(PFjval_Hopt2)), \
'{:3.2e}'.format(kexj_Hopt2), \
'{:3.2f}'.format(math.log10(PFjval_Hopt3)), \
'{:3.2e}'.format(kexj_Hopt3),\
'{:3.2f}'.format(math.log10(PFjval_Hopt4)), \
'{:3.2e}'.format(kexj_Hopt4), \
'{:3.2f}'.format(math.log10(PFjval_Hopt5)), \
'{:3.2e}'.format(kexj_Hopt5), \
'{:3.2f}'.format(math.log10(PFjval_Hopt6)), \
'{:3.2e}'.format(kexj_Hopt6), \
'{:10.5f}'.format(PFpower_Hopt0), \
'{:10.5f}'.format(PFpower_Hopt1), \
'{:10.5f}'.format(PFpower_Hopt2), \
'{:10.5f}'.format(PFpower_Hopt3), \
'{:10.5f}'.format(PFpower_Hopt4), \
'{:10.5f}'.format(PFpower_Hopt5), \
'{:10.5f}'.format(PFpower_Hopt6)
                        print >> d, '{:10d}'.format(renum_m), '{:10s}'.format(mykeys_resname[r]), \
'{:10.1f}'.format(frac_NH_CO), \
'{:10.1f}'.format(frac_NH_wat), \
'{:10.1f}'.format(frac_non_NHB), \
'{:10.1f}'.format(frac_NH_SC), \
'{:10.1f}'.format(frac_CO_SC), \
'{:10.1f}'.format(frac_CO_wat), \
'{:10.1f}'.format(frac_non_AllHB), \
'{:10.1f}'.format(frac_non_HB_count)

#############################################
def HBstat_print(mykeys_resname, kint_resnum_dic, base, timeUnit, PRO_list):


    mykeys = mykeys_resname.keys()
    mykeys.sort()

    Hbond_list = [line for line in glob.glob("../HbondALL/hbond.*[0-9]")]
    #Hbond_list = [line for line in glob.glob("HbondALL/hbond.*[0-9]")]
    Hbond_list.sort(key = lambda line:int(line.split('.')[-1]))
    snapshot_list = [line.split('.')[-1].lstrip('0') for line in Hbond_list]
    total_snap = len(Hbond_list)

    mykeys = mykeys_resname.keys()
    mykeys.sort()
    
    Hbond_dic_NH_wat = defaultdict(defaultdict)
    Hbond_dic_CO_wat = defaultdict(defaultdict)
    Hbond_dic_NH_CO =  defaultdict(defaultdict) 
    Hbond_dic_NH_SC =  defaultdict(defaultdict)
    Hbond_dic_CO_SC =  defaultdict(defaultdict)

    count = 0
    for hbond_info in Hbond_list:
        count += 1
        percent = count*1.0/total_snap 
        drawProgressBar(percent)
        snap_no = hbond_info.split('.')[-1].lstrip('0')
        with open(hbond_info, 'r') as f:                                                                                                  
            for _ in f:
                            #WAT 99047.water O   ASN 1291 O          WAT 99047.water H1   2.885  2.013
                            #ARG 18 NE           WAT 42617.water O   ARG 18 HE            2.719  1.722
                m = re.match("^(\S+) (\S+) (\S+)\s+(\S+) (\S+) (\S+)\s+(\S+) (\S+) (\S+) ", _)
                #                1      2     3      4     5      6       7    8     9 
                if m and not re.search("HEM", _):
                    Dres = m.group(1); Datom = m.group(3)
                    Ares = m.group(4); Aatom = m.group(6)
                    Hres = m.group(7); Hatom = m.group(9)
                    Dresnum = int(m.group(2).replace(".water", ""))
                    Aresnum = int(m.group(5).replace(".water", ""))
                    Hresnum = int(m.group(8).replace(".water", ""))

                    #print Dresnum, Aresnum, Hresnum
                    for r in mykeys:
                        #############
                        # water-amide
                        #############
                        #ARG 18 N            WAT 42617.water O   ARG 18 H             2.719  1.722  
                        #       Datom        Ares                Hresnum Hatom
                        #--------------------------------------------------------------------------
                        if Ares == "WAT": # NH[D,H]...O-HH[A]
                            if Hresnum == r and Hatom == "H" and Datom == "N": 
                                Hbond_dic_NH_wat[r][snap_no] = 1
                        #WAT 37753.water O   THR 752 O           WAT 37753.water H1   2.709  1.762  
                        # Dres              Aresnum  Aatom                     Hatom
                        #--------------------------------------------------------------------------
                        elif Dres == "WAT":  #O[A]...H1-OH[D,H] 
                            if Aresnum == r and Aatom == "O" and Hatom == "H1":
                                Hbond_dic_CO_wat[r][snap_no] = 1
                        #############
                        # amide-amide
                        #############
                        # H-bond donor : NH
                        elif Hresnum == r and Hatom == "H" and Datom == "N": 
                            if Aatom == "O":
                                Hbond_dic_NH_CO[r][snap_no] = 1
                            else:
                                Hbond_dic_NH_SC[r][snap_no] = 1
                        # amide-amide
                        # H-bond acceptor : backbone carbonyl O
                        elif Aresnum == r and Aatom == "O" and Datom != "N": 
                            Hbond_dic_CO_SC[r][snap_no] = 1


    mykeys = mykeys_resname.keys()
    mykeys.sort()


    with open("%sC_pD%s_base%s_timeUnit%s.wat.logistic.HBstat" % (myTempC, pD_corr, str('{:1.0e}'.format(base)), timeUnit.replace('"', '')), 'w') as c:
        #print >> c, "ResNum\tResidue\tTotalSnap\tkint\tPFj\tNH:O=C\tNH:sc\tC=O:sc\tNH:O-HH\tC=O:H1-OH\tFold\tUnfold\tTot_Hbond"
        print >> c, '{:10s}'.format("ResNum"), '{:10s}'.format("Residue"),\
'{:10s}'.format("frac_NH_CO"), \
'{:10s}'.format("frac_NH_wat"), \
'{:10s}'.format("frac_non_NHbond"), \
'{:10s}'.format("frac_NH_SC"), \
'{:10s}'.format("frac_CO_SC"), \
'{:10s}'.format("frac_CO_wat"),\
'{:10s}'.format("frac_none_Hbond")

        for r in mykeys:                                                                                                               
            renum_m = int(r) + int(offset)
            Hbond_NH_wat = 0 
            Hbond_CO_wat = 0
            Hbond_NH_CO = 0
            Hbond_NH_SC = 0
            Hbond_CO_SC = 0
            for i in snapshot_list:
                if Hbond_dic_NH_wat[r].has_key(i): 
                    Hbond_NH_wat += 1 
                elif Hbond_dic_CO_wat[r].has_key(i): 
                    Hbond_CO_wat += 1 
                elif Hbond_dic_NH_CO[r].has_key(i): 
                    Hbond_NH_CO += 1 
                elif Hbond_dic_NH_SC[r].has_key(i): 
                    Hbond_NH_SC += 1 
                elif Hbond_dic_CO_SC[r].has_key(i): 
                    Hbond_CO_SC += 1 

            frac_NH_CO =  100.0*(Hbond_NH_CO)/total_snap
            frac_NH_wat = 100.0*(Hbond_NH_wat)/total_snap
            #
            frac_non_NHbond = 100.0 - frac_NH_CO - frac_NH_wat
            #
            frac_NH_SC = 100.0*(Hbond_NH_SC)/total_snap
            frac_CO_SC = 100.0*(Hbond_CO_SC)/total_snap 
            #
            frac_CO_wat = 100.0*(Hbond_CO_wat)/total_snap
            #
            frac_none = 100.0 - frac_NH_CO - frac_NH_wat - frac_NH_SC - frac_CO_SC - frac_CO_wat
                                                                                                                                   
            #
            if (r + 1 in PRO_list):
                print >> c, '{:10d}'.format(renum_m), '{:10s}'.format(mykeys_resname[r]), \
'{:10.1f}'.format(frac_NH_CO), \
'{:10.1f}'.format(frac_NH_wat), \
'{:10.1f}'.format(frac_non_NHbond), \
'{:10.1f}'.format(frac_NH_SC), \
'{:10.1f}'.format(frac_CO_SC), \
'{:10.1f}'.format(frac_CO_wat), \
'{:10.1f}'.format(frac_none)
                print >> c, '{:10s}'.format('      #'+str(renum_m + 1)), '{:10s}'.format('PRO')   
            else:
                if r not in PRO_list:
                    print >> c, '{:10d}'.format(renum_m), '{:10s}'.format(mykeys_resname[r]), \
'{:10.1f}'.format(frac_NH_CO), \
'{:10.1f}'.format(frac_NH_wat), \
'{:10.1f}'.format(frac_non_NHbond), \
'{:10.1f}'.format(frac_NH_SC), \
'{:10.1f}'.format(frac_CO_SC), \
'{:10.1f}'.format(frac_CO_wat), \
'{:10.1f}'.format(frac_none)



if __name__ == '__main__':
    
    import sys

    if len(sys.argv) != 4:
        print
        print "usage: python HDX-correlation-MD.py [1] [2] [3]"
        print "[1] full-path of PDBfile"
        print "[2] offset of residue number"
        print "[3] ExpCondition.dat file"
        print
        print "         pD_corr = 7.4     "
        print "         myTempC = 25.0    "
        print "         timeUnit = \"min\"  "
        print "         base = 1.0e+6"
        print
        print "         Hbond = 1 (NH_CO-NH_wat), 2 (NH_CO+NH_SC-NH_wat), 3 (NH_CO+NH_SC+CO_SC-NH_wat-CO_wat)"
        sys.exit()
    else:
        PDBfile = sys.argv[1]
        if not re.search("/", PDBfile):
            print
            print "provide a full path of PDBfile, not just PDB file name"
            print
            sys.exit()
        else:
            offset = sys.argv[2]        
            ExpConditions = sys.argv[3]
            pD_corr =  0.0
            myTempC = 0.0
            timeUnit = "min"
            base = 0.0e+0
            with open(ExpConditions, 'r') as f:                                                                                                        
                for _ in f:
                    x = _.split('=')
                    if x[0].strip() == 'pD_corr':
                        pD_corr = float(x[1].strip())
                    elif x[0].strip() == 'myTempC':
                        myTempC = float(x[1].strip())
                    elif x[0].strip() == 'timeUnit':
                        timeUnit = str(x[1].strip())
                    elif x[0].strip() == 'base':
                        base = float(x[1].strip())
            mykeys_resname, kint_resnum_dic, PRO_list = sequence_kint(pD_corr, myTempC, timeUnit, PDBfile, offset)                                    
            Hbond_wat_prot_MD_logistic(PDBfile, mykeys_resname, kint_resnum_dic, base, timeUnit, PRO_list, scienceFormat = 1)
