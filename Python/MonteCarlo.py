"""
CODING SAMPLE (Python/SQLite code for Monte Carlo Simulation)}
Aim:
Select the lowest energy sample representing the important structures via Monte Carlo simulation and archive/retrieve them into/from database
for re-usability.  Such program is available as COREX but restricted for-profit
use, thus implemented it from scratch by referring to literature/patent.
"""
#\begin{minted}[linenos=true,fontsize=\scriptsize]{python}
import re, sys, math, posix, gzip, os
import subprocess
import tempfile
import glob
import math
import datetime
import random
import pickle
import shutil
import sqlite3 as lite
import time

main_dir = os.getcwd().replace("/Script_py", "")
script_dir = os.getcwd()

constR_kcal = 1.9858775/1000. # unit: kcal/mol/K

res_thermo_dic = {
# unit: cal/mol/K
# residue: [ASA_ap, ASA_p, S_bu2ex, S_ex2u, S_bb],
'ALA':[70.0, 36.1, 0.0, 0.0, 4,1],
'ARG':[87.1, 126.1, 7.11, -0.84, 3.4],
'ASN':[38.1, 104.0, 3.29, 2.24, 3.4],
'ASP':[42.1, 95.0, 2.00, 2.16, 3.4],
'CYS':[30.3, 75.05, 3.55, 0.61, 3.4],
'GLN':[65.0, 121.6, 5.02, 2.12, 3.4],
'GLU':[71.1, 94.3, 3.53, 2.27, 3.4],
'GLY':[26.2, 43.12, 0.0, 0.0, 6.5],
'HIS':[90.0, 68.0, 3.44, 0.79, 3.4],
'ILE':[110.7, 10.9, 1.74, 0.67, 2.18],
'LEU':[122.3, 27.5, 1.63, 0.25, 3.4],
'LYS':[101.3, 79.0, 5.86, 1.02, 3.4],
'MET':[104.6, 64.0, 4.55, 0.58, 3.4],
'PHE':[186.8, 36.5, 1.40, 2.89, 3.4],
'PRO':[100.8, 15.6, 0.0, 0.0, 0.0],
'SER':[55.1, 81.9, 3.68, 0.55, 3.4],
'THR':[79.5, 41.1, 3.31, 0.48, 3.4],
'TRP':[184.5, 52.3, 2.74, 1.15, 3.4],
'TYR':[175.8, 71.1, 2.78, 3.12, 3.4],
'VAL':[88.7, 17.8, 0.12, 1.29, 2.18],
}

def COREX_partition(win_size, tot_seq, min_unit, print_option = 0):

    units = int(math.ceil(tot_seq/win_size))
    left =  tot_seq % win_size
    if left < min_unit:
        left = win_size + left

    if print_option == 1:
        print                                                            
        print "COREX partition ======================"
        print "......window size of folding unit = %d" % (win_size)
        print "......total residue in PDB = %d" % (tot_seq)
        print "......minimum residues in folding unit = %d" % (min_unit)
        print

    S_all = range(min_unit, win_size + (min_unit - 1) + 1)
    

    partition = []
    large_sum = win_size + left
    for i in S_all: 
        first_FU = i 
        diff = abs(large_sum - i)
        if diff <= win_size:
            last_FU = large_sum - i
            if last_FU < min_unit:
                last_FU += win_size
        else:
            last_FU = large_sum - i - win_size
            if last_FU < min_unit:
                last_FU += win_size
                

        middle = tot_seq - first_FU - last_FU
        FUs = []
        for j in range(middle/win_size):
            FUs.append("%s" % str(win_size))
        string = "%d %s %d" % (first_FU, ' '.join(FUs), last_FU)
        partition.append(string)
     
    uniq_partition = set(partition)
    FUs_partition = []
    j = 0 
    last_i = 0
    for p in uniq_partition:
        j += 1 
        last_i += 2**len(p.split())-2 
        if print_option == 1:
            print "P%02d: %2d FUs | %3.2e m_state | %s" % (j, len(p.split()), 2**len(p.split())-2, p)
        FUs_partition.append(len(p.split()))

   
    tot_microstate = 0
    for k in range(len(FUs_partition)):
        tot_microstate += (2**FUs_partition[k] - 2)

    if print_option == 1:
        print                                                         
        print "     Total partition = %d" % (len(uniq_partition))
        print "     Total microstate = %3.2e" % (2 + tot_microstate) 
        print

    return list(uniq_partition)



def get_native_info_dic(native_pdb, vadar_parm_dir):
    
    #
    # get native sequence and residue list
    #
    native_info_dic = {}
    # {'res_num':[1,2,....,129], # sequence number
    #  'res_name':[ALA, VAL, ...., TYR], # residue list
    #  'pops_asa': [ASA_apolar, ASA_polar], 
    #  'surf_asa': [ASA_apolar, ASA_polar]}

    f = open(native_pdb, 'r')
    native_res_num = []
    native_res_name = []
    for _ in f:
        if re.search("^ATOM", _) and re.search("CA", _):
            #0         1         2         3         4                                      
            #01234567890123456789012345678901234567890
            #ATOM    976  N   CYS A 127     -11.912  23.409   2.685  1.00 12.41           N
            #
            native_res_num.append(int(_[22:25+1]))
            native_res_name.append(_[17:19+1])
    f.close()
    native_info_dic['res_num'] = native_res_num
    native_info_dic['res_name'] = native_res_name

    #
    # Feb29, 2012
    # consider P_{ex,f,j}, i.e. amide group (can be amide hydrogen or amide nitrogen)
    # is solvent accessible in the native strcture in the following two cases:
    #
    # case1) amide group is intrinsically solvent exposed in the native structure
    # =====> construct intrinsic exposed residue list in the native_info_dic
    # by comparing to polar-ASA of unfolded peptide model values
    # residue-wise ASA = peptide model ASA  
    #
    # case2) amide group is solvent exposed where the residue is in the folded unit
    # but due to removal of unfolded part of structure in microstate 
    # =====> for the residues not in the intrinsic_exposed_residue list,
    # and residue belongs to folded unit (fold_flag = 0),
    # calculate native-ASA < residue-wise ASA < peptide model ASA  
    #

    # ASA by VADAR
    # ipark: change this dir to temporary dir at that moment.
    ##########################
    tempfile_tempdir = tempfile.mkdtemp()
    os.chdir(tempfile_tempdir) 
    """
    if not os.path.exists("vadar_run"):
        os.mkdir("vadar_run")
    os.chdir("vadar_run")
    """
    ##########################
    if not os.path.exists("vadar.parms"):
        posix.system("ln -s %s/vadar.parms vadar.parms" % vadar_parm_dir)

    # native ASA by VADAR     
    vadar_in ="""#!/bin/bash
vadar -f %s -o native
rm -rf vadar.*.*
""" % (native_pdb)
    f = open("vadar.run", 'w')                                                                                                   
    f.write(vadar_in)
    f.close()
    #posix.system("source vadar.run >&/dev/null")
    posix.system("chmod +x vadar.run")
    posix.system("./vadar.run > /dev/null 2>&1")
    #
    g = open("native.stats", 'r')
    try:
        for _ in g:                                      
            if re.search("Exposed nonpolar", _):        
                vadar_a = (_.split('|')[2]).split()[0]
            elif re.search("Exposed polar", _):    
                vadar_p1 = (_.split('|')[2]).split()[0]
            elif re.search("Exposed charg", _):    
                vadar_p2 = (_.split('|')[2]).split()[0]
    finally:
        g.close()

    native_info_dic['vadar_asa'] = [ float(vadar_a), float(vadar_p1)+float(vadar_p2) ]


    # native fractional ASA by VADAR  
    from collections import defaultdict

    n = open("native.main", 'r')
    nat_frac_asa_dic = defaultdict(list)
    found = 0
    for _ in n:
        if found == 1:                                 
            j = int(_[:3])
            j_name = _[5:7+1]
            frac_asa_j = "%3.2f" % (float(_[35:39+1]))
            nat_frac_asa_dic[j].append(j_name)
            nat_frac_asa_dic[j].append(float(frac_asa_j))
        else: 
            if re.search("^------------", _):
                found = 1
    n.close()

    return native_info_dic, nat_frac_asa_dic
    # Clean up the directory
    shutil.rmtree(path, ignore_errors=True)
    os.chdir(script_dir)




def fetchsome(cursor, arraysize):
    # a geneator that simplified the use of fetchmany
    while True:
        results = cursor.fetchmany(arraysize)
        if not results: break
        for result in results:
            yield result




def COREX_microstate_ensemble_selection_via_MC_OnTheFly(partition_string_list, \
native_pdb, W, T, exposure_cutoff, sub_ensemble, win_size, partition_calc, ASAcalc_dir):

    from collections import defaultdict
    from itertools import product, repeat
    import numpy


    # partitionIfo dictionary:
    #   key = partitionNumber
    #   values = (partitionSeq, FUs) 
    partitionInfo = defaultdict(list)
    s1 = [(p+1, partition_string) for p, partition_string in enumerate(partition_string_list)]
    s2 = [(p+1, len(partition_string.split())) for p, partition_string in enumerate(partition_string_list)]
    for k, v in s1:
        partitionInfo[k].append(v)
    for k, v in s2:
        partitionInfo[k].append(v)
    
    p = int(partition_calc)
    print p
    prefix = native_pdb.split("/")[-2]


    if not os.path.exists("/tmp"):
        os.mkdir("/tmp")

    import posix

    tmp_user = "/tmp/%s" % (os.environ['USER'])
    if not os.path.exists(tmp_user):
        os.mkdir(tmp_user)
         
    tmp_dir = "%s/%s" % (tmp_user, prefix)
    if not os.path.exists(tmp_dir):
        os.mkdir(tmp_dir) 

    dir = os.path.join(tmp_dir, "partition%d_win%s_W%s_%s" % (p, str(win_size), str(W), ASAcalc_dir))
    if not os.path.exists(dir):
        os.mkdir(dir) 

    MC_DB_dir_base = "%s/%s/%s" % (main_dir, prefix, ASAcalc_dir)
    if not os.path.exists(MC_DB_dir_base):
        os.mkdir(MC_DB_dir_base)
    MC_DB = os.path.join(MC_DB_dir_base, "%s_partition%d_win%s_W%s.db" % (prefix, p, str(win_size), str(W)))

    print MC_DB

    #with lite.connect(MC_DB, detect_types=lite.PARSE_DECLTYPES) as con:
    ############################################
    with lite.connect(MC_DB) as con:
        cur = con.cursor()
        #print "with lite", cur
        cur.execute("PRAGMA journal_mode=WAL;")
        con.execute("""CREATE TABLE IF NOT EXISTS MC_DB (
mstateID INTEGER PRIMARY KEY, 
MC_step INTEGER, 
delG_i REAL, 
AcceptP_i REAL, 
FUsum INTEGER, 
MCsampleCycle INTEGER,
Accum_mstate INTEGER, 
ExpRes TEXT);""")


    # save native PDB coordinates for generating microstates ensemble
    native_pdb_per_res = {}
    f = open(native_pdb, 'r')
    for _ in f:
        if re.search("^ATOM", _): 
            res = int(_[22:25+1])
            native_pdb_per_res.setdefault(res, []).append(_.strip())
    f.close()

    # native ASA and frac_asa
    native_info_dic, nat_frac_asa_dic = get_native_info_dic(native_pdb, vadar_parm_dir)  
    #print nat_frac_asa_dic

    ############################################
    os.chdir(dir)
    ############################################
    #                                                                                                                                         
    # free energy of the completely unfolded structure from the ASA and Sconf table
    #
    #   res_thermo_dic = { (unit: cal/mol/K)
    #    residue: [ASA_ap, ASA_p, S_bu2ex, S_ex2u, S_bb],...}
    #              0         1        2       3     4
    #
    nf_ASA_apol = 0.0; nf_ASA_pol = 0.0 
    nf_S_bu2ex = 0.0; nf_S_ex2u = 0.0; nf_S_bb = 0.0
    del_nf_G_apol = 0.0; del_nf_G_pol = 0.0 
    del_nf_G = 0.0
    del_nf_ASA_apol = 0.0; del_nf_ASA_pol = 0.0 
    reskeys = native_pdb_per_res.keys()
    reskeys.sort()
    for j in reskeys:
        rname = native_info_dic['res_name'][j-1]
        nf_ASA_apol += res_thermo_dic[rname][0]
        nf_ASA_pol += res_thermo_dic[rname][1]
        nf_S_bu2ex += res_thermo_dic[rname][2]
        nf_S_ex2u += res_thermo_dic[rname][3]
        nf_S_bb += res_thermo_dic[rname][4]

    ASA_apol_nat = native_info_dic['vadar_asa'][0]
    ASA_pol_nat = native_info_dic['vadar_asa'][1]

    del_nf_ASA_apol = nf_ASA_apol - ASA_apol_nat
    del_nf_ASA_pol = nf_ASA_pol - ASA_pol_nat
    del_nf_G_apol = -8.44 * del_nf_ASA_apol + 0.45 * del_nf_ASA_apol * (T - 333.0) - T * (0.45 * del_nf_ASA_apol * math.log(T/385.0)) 
    del_nf_G_pol = 31.44 * del_nf_ASA_pol - 0.26 * del_nf_ASA_pol * (T - 333.0) - T * (-0.26 * del_nf_ASA_pol * math.log(T/335.0)) 
    del_nf_G = del_nf_G_apol + del_nf_G_pol  - T * W * (nf_S_bu2ex + nf_S_ex2u + nf_S_bb)
    del_nf_G_kcal = del_nf_G/1000.
    print "W = %f, T = %f (K)" % (W, T)
    print "Ref: delG_nf for MC selection: %f (kcal/mol)" % (del_nf_G_kcal)
    
    if not os.path.exists("vadar.parms"):
        posix.system("ln -s %s/vadar.parms vadar.parms" % vadar_parm_dir)


    partitionSeq = partitionInfo[p][0]  # 7 8 8 8 8 8 4 (each number represents a number of residues in a FU
    FUs = partitionInfo[p][1]           # total number of FUs

    print partitionSeq
    # microstate generator
    #microstate_dic = defaultdict(int)
    last_i = 2**FUs
    # randomly select a integer out of full range (1, 2**FUs - 2)
    # instead of drwing one random number one at a time,
    # sample multiple random numbers one at a time to save time.
    # here convert list to numpy-array
    # then convert an integer to binary via '{0:0FUs0}'.format{int(random.randint())}
    
    # since MC selection occurs seldomly, 
    # drawing a desired sub_ensemble numbers within a whole range of integer-space (1, last_i) at once is impossible,
    # thus split the whole range of integer space into many,
    # and drawing the numbers repeatedly to accumulate the desired sub_ensemble number of microstates
    #
    # desire to accumulate 10^5 sampling per partition,
    # split a whole integer space into 10, 
    # if sample 10^4 from per window/partition, can accumulate 10^5 sampling per partition
    # given the seldomness of MC selection, sampling including a backup numbers for rejection
    # so, per window/partition, 10^4 * 10 = 10^6
    MC_step = 0
    accum_mstate = 0
    sample_cycle = 0

    """
    # split the whole integer space into 10 sub-cycle;
    # sample 10^5 random numbers in a int_space (DXCOREX = 80K/partition)
    # expected to MC selection about 10^4
    # thus accumulate 10^5 (10^4/cycle * 10 cycles) sub-ensembles
    integer_space_split = [(1+(x-1)*(last_i)/10, x*last_i/10) for x in xrange(1, 10+1)]
    random.shuffle(integer_space_split)
    #integer_space_split.reverse()
    #print integer_space_split
    #sys.exit()
    for int_space in integer_space_split:
        sample_cycle += 1 
        print sample_cycle, int_space[0], int_space[1]

        mstateIDarray = numpy.array(random.sample(xrange(int_space[0], int_space[1]), int(sub_ensemble)*10))
    """
    mstateIDarray = numpy.array(random.sample(xrange(1, last_i), int(sub_ensemble)))

    for i in mstateIDarray:                                                                                                              
        
        i = int(i) #<--------------------------key to solve sqlite3.InterfaceError: Error binding parameter 0 - probably unsupported type.

        if accum_mstate == sub_ensemble:
            print "sub_ensemble %d has been accumulated...done" % (sub_ensemble)
            break
                                                                                                                                         
        MC_step +=1 
        #print MC_step,
                                                                                                                                        
        int2bin = eval("'{0:0%db}'.format(i)" % FUs)
        FUsum = sum([int(f)  for f in int2bin])
        #microstate_dic[FUsum] += 1      # this information is used to trace back distribution of energy and multi-body of folding units
        #print i, int2bin, sum(FUsum)
        #print int2bin
    
        ##################################
        fold_flag = []
        # here generate a full fold-flag string by product of int2bin(1/0 comb) x partitionSeq (7 8 8 ...4)
        for s, bit in enumerate(int2bin):
            if int(bit) == 1: # fold
                fold_flag.extend([z for z in repeat(1, int(partitionSeq.split()[s]))])
            else: # unfold
                fold_flag.extend([z for z in repeat(0, int(partitionSeq.split()[s]))])
        ##################################
        #print fold_flag
                                                                                                                                        
                                                                                                                                        
        # per microstate - calculate delG_i and select based on MC criteria
        last_TER = 1
        #temp = os.path.join(dir, "mstate_%d.pdb" % i)
        temp = "mstate_%d.pdb" % i
        reskeys = native_pdb_per_res.keys()
        reskeys.sort()
        tf = open(temp, 'w')
        for j in reskeys: # residue, j                                    
            if fold_flag[j - 1] != 0:
                tf.write('\n'.join(native_pdb_per_res[j]) + '\n')
                last_TER = 0
            elif fold_flag[j - 1] == 0:
                # add TER card in PDB if not continuous in residue number
                if last_TER == 0:
                    tf.write('TER\n')
                    last_TER = 1
        tf.close()
        
        # vadar
        mstate_pdb = temp
        # native ASA by VADAR     
        vadar_in ="""#!/bin/bash
vadar -f %s -o vadar
rm -rf vadar.*.*
""" % (mstate_pdb)
        f = open("vadar.run", 'w')                                                                                                                                                                        
        f.write(vadar_in)
        f.close()

        if os.path.exists(mstate_pdb):
            #posix.system('source vadar.run >&/dev/null')
            posix.system("chmod +x vadar.run")
            posix.system("./vadar.run > /dev/null 2>&1")
                                                                                                                                      
            if not os.path.exists("vadar.stats"):                                                                                                                                                             
                time.sleep(5)
            elif os.path.exists("vadar.stats"):
                g = open("vadar.stats", 'r')
                for _ in g:                                                                                                           
                    if re.search("Exposed nonpolar", _):        
                        vadar_a = (_.split('|')[2]).split()[0]
                    elif re.search("Exposed polar", _):    
                        vadar_p1 = (_.split('|')[2]).split()[0]
                    elif re.search("Exposed charg", _):    
                        vadar_p2 = (_.split('|')[2]).split()[0]
                g.close()
                ASA_apol_comp_i = float(vadar_a)
                ASA_pol_comp_i = float(vadar_p1) + float(vadar_p2)
                #print ASA_apol_comp_i, ASA_pol_comp_i
                                                                                                                                                                                                             
            
            #                                                                                                                               
            # fractional ASA
            #
            if not os.path.exists("vadar.main"):
                time.sleep(5)
            elif os.path.exists("vadar.main"):
                m = open("vadar.main", 'r')
                mstate_fasa_dic = defaultdict(list)
                found = 0
                for _ in m:
                    if found == 1:                                 
                        if not _[:3] == "-1 ":
                            j = int(_[:3])                               
                            j_name = _[5:7+1]
                            frac_asa_j = "%3.2f" % (float(_[35:39+1]))
                            mstate_fasa_dic[j].append(j_name)
                            mstate_fasa_dic[j].append(float(frac_asa_j))
                    else: 
                        if re.search("^------------", _):
                            found = 1
                m.close()
                                                                                                                         
                keys = mstate_fasa_dic.keys()
                keys.sort()
                res_list = []
                for res in keys:
                    #
                    # per microstate, save residue list being exposed at complementary region (excluding intrinsic exposed residue)
                    # i.e. residues buried in the native (nat_fasa < 0.6);
                    # but become exposed (fasa >= 0.6) at the complementary region (frac_asa_diff between mstate - native) by 0.3
                    frac_diff = float(mstate_fasa_dic[res][1]) - float(nat_frac_asa_dic[res][1])
                    if frac_diff >= exposure_cutoff/2. and mstate_fasa_dic[res][1] >= exposure_cutoff and nat_frac_asa_dic[res][1] < exposure_cutoff:
                        res_list.append(res)
                if len(res_list) > 0:
                    res_list_all = ','.join([str(r) for r in res_list])
                else:
                    res_list_all = 'NONE'
                                                                                               
        else:
            time.sleep(5)
                                                                                                                                                                                                         
        os.remove(mstate_pdb)

        # initialization per microstate                                                                                                  
        S_bu2ex_i = 0.0; S_ex2u_i = 0.0; S_bb_i = 0.0
        ASA_apol_uf_i = 0.0; ASA_pol_uf_i = 0.0                                                                                                               
        ASA_apol_i = 0.0; ASA_pol_i = 0.0 
        G_apol_i = 0.0; G_pol_i = 0.0   
        deltaG = 0.0    
        dletaG_kcal = 0.0
        del_ASA_apol_i = 0.0; del_ASA_pol_i = 0.0
        #
        for j, jval in enumerate(fold_flag):
            if jval == 0: # unfolded residue j 
                #
                #res_thermo_dic = { 
                #         0       1      2       3       4          
                #  ALA: [ASA_ap, ASA_p, S_bu2ex, S_ex2u, S_bb], ..., 
                #  VAL: [ASA_ap, ASA_p, S_bu2ex, S_ex2u, S_bb]}
                #                                                                          
                res_name = native_info_dic['res_name'][j]
                ASA_apol_uf_i += res_thermo_dic[res_name][0]
                ASA_pol_uf_i += res_thermo_dic[res_name][1]
                S_bu2ex_i += res_thermo_dic[res_name][2]
                S_ex2u_i += res_thermo_dic[res_name][3]
                S_bb_i += res_thermo_dic[res_name][4]
        #
        # HERE WE DO NEED NATIVE ASAs for the MC calculation
        #
        del_ASA_apol_i = ASA_apol_uf_i + ASA_apol_comp_i - ASA_apol_nat
        del_ASA_pol_i = ASA_pol_uf_i + ASA_pol_comp_i - ASA_pol_nat
        del_G_apol_i = -8.44 * del_ASA_apol_i + 0.45 * del_ASA_apol_i * (T - 333.0) - T * (0.45 * del_ASA_apol_i * math.log(T/385.0)) 
        del_G_pol_i = 31.44 * del_ASA_pol_i - 0.26 * del_ASA_pol_i * (T - 333.0) - T * (-0.26 * del_ASA_pol_i * math.log(T/335.0)) 
        del_G_i = del_G_apol_i + del_G_pol_i - T * W * (S_bu2ex_i + S_ex2u_i + S_bb_i)
        del_G_i_kcal = del_G_i/1000.
        #print "current G_i = %f" % float(G_i)
        #K_i = math.exp(-1.0*delG_i/(constR * T))
        #formatK_i = "%e" % K_i
        #print "i = %d, delG_i = %f, K_i = %e" % (i, delG_i, K_i)
                                                                                                                                          
                                                                                                                                                                                                         
                                                                                                                                                                                                         
        #################################                                               
        ## MONTE CARLO SELECTION ######## no update of new reference delG, just stick to use delG_unfold
        #################################

        delG_ref_kcal = del_nf_G_kcal
        deltaG_kcal = del_G_i_kcal - delG_ref_kcal

        #if deltaG_kcal < 0.0e0:                                                                                        
        #if deltaG_kcal <= 1.0e-7:
        if deltaG_kcal <= 1.0e-3:
            accept_prob = 1.0
            accum_mstate += 1
            """
            if MC_step % 100 == 0:
                print "MC_step = %d | curr = %f | ref = %f" %  (MC_step, del_G_i_kcal, delG_ref_kcal)
            """
            info_tuple = (i, MC_step, del_G_i_kcal, accept_prob, FUsum, sample_cycle, accum_mstate, res_list_all)
            """
            print info_tuple
            """
            cur.execute("INSERT INTO MC_DB VALUES(?, ?, ?, ?, ?, ?, ?, ?)", info_tuple)
            con.commit()
            # update new ref ==> no update, keep using del_nf_G_kcal
            delG_ref_kcal = del_nf_G_kcal
        else:
            boltzman = math.exp(-1.0*deltaG_kcal/(constR_kcal * T))
            rand = random.random()
            if boltzman > rand:
                accept_prob = boltzman     
                accum_mstate += 1
                """
                if MC_step % 100 == 0:
                    print "MC_step = %d | curr = %f | ref = %f" %  (MC_step, del_G_i_kcal, delG_ref_kcal)
                """
                info_tuple = (i, MC_step, del_G_i_kcal, accept_prob, FUsum, sample_cycle, accum_mstate, res_list_all)
                """
                print info_tuple
                """
                cur.execute("INSERT INTO MC_DB VALUES(?, ?, ?, ?, ?, ?, ?, ?)", info_tuple)
                con.commit()
                # update new ref ==> no update, keep using del_nf_G_kcal 
                delG_ref_kcal = del_nf_G_kcal
            else:
                """
                if MC_step % 100 == 0:
                    print "MC_step = %d | curr = %f | ref = %f" %  (MC_step, del_G_i_kcal, delG_ref_kcal)
                """
                # no update
                delG_ref_kcal = del_nf_G_kcal



def delG_unfold_chk(native_pdb, T, user_input_W):

    from collections import defaultdict
    from itertools import product, repeat
    import numpy


    native_pdb_per_res = defaultdict(list)
    f = open(native_pdb, 'r')
    for _ in f:
        if re.search("^ATOM", _) and re.search("CA", _):
            res = int(_[22:25+1])
            #atom = int(_[5:11+1])
            #native_pdb_per_res.setdefault(res, []).append(_.strip())
            native_pdb_per_res[res].append(_.strip())
    f.close()

    native_info_dic, nat_frac_asa_dic = get_native_info_dic(native_pdb, vadar_parm_dir)
                                                                                                                                               
    #                                                                                                                                         
    # free energy of the completely unfolded structure from the ASA and Sconf table
    #
    #   res_thermo_dic = { (unit: cal/mol/K)
    #    residue: [ASA_ap, ASA_p, S_bu2ex, S_ex2u, S_bb],...}
    #              0         1        2       3     4
    #
    prefix = (os.path.basename(native_pdb)).replace(".pdb", "")
    ###f = open("%s_delG_unfold.dat" % prefix, 'w')

    nf_ASA_apol = 0.0; nf_ASA_pol = 0.0 
    nf_S_bu2ex = 0.0; nf_S_ex2u = 0.0; nf_S_bb = 0.0
    del_nf_G_apol = 0.0; del_nf_G_pol = 0.0 
    del_nf_G = 0.0
    del_nf_ASA_apol = 0.0; del_nf_ASA_pol = 0.0 
    reskeys = native_pdb_per_res.keys()
    reskeys.sort()
    for j in reskeys:
        rname = native_info_dic['res_name'][j-1]
        #print j, rname, res_thermo_dic[rname]
        nf_ASA_apol += res_thermo_dic[rname][0]
        nf_ASA_pol += res_thermo_dic[rname][1]
        nf_S_bu2ex += res_thermo_dic[rname][2]
        nf_S_ex2u += res_thermo_dic[rname][3]
        nf_S_bb += res_thermo_dic[rname][4]

    #print "unf_ASA_a = %f; unf_ASA_p = %f" % (nf_ASA_apol, nf_ASA_pol)
    ASA_apol_nat = native_info_dic['vadar_asa'][0]
    ASA_pol_nat = native_info_dic['vadar_asa'][1]

    del_nf_ASA_apol = nf_ASA_apol - ASA_apol_nat
    del_nf_ASA_pol = nf_ASA_pol - ASA_pol_nat
    #Cp = 0.44 * del_nf_ASA_apol - 0.25 * del_nf_ASA_pol
    #H60 = 31.4 * del_nf_ASA_pol - 8.44 * del_nf_ASA_apol
    del_nf_G_apol = -8.44 * del_nf_ASA_apol + 0.45 * del_nf_ASA_apol * (T - 333.0) - T * (0.45 * del_nf_ASA_apol * math.log(T/385.0)) 
    del_nf_G_pol = 31.44 * del_nf_ASA_pol - 0.26 * del_nf_ASA_pol * (T - 333.0) - T * (-0.26 * del_nf_ASA_pol * math.log(T/335.0)) 
    print "PDB       W(S_conf)    delG_nf(kcal/mol)"
    ###f.write("PDB       W(S_conf)    delG_nf(kcal/mol)\n")
    
    if user_input_W != 0:
        W = user_input_W
        for W in [W + x * 0.005 for x in range(-5, 5)]:                                                                      
            del_nf_G = del_nf_G_apol + del_nf_G_pol  - T * W * (nf_S_bu2ex + nf_S_ex2u + nf_S_bb)                   
            if W == user_input_W:
                print "%s\t\t%3.3f\t\t%3.3f ***" % ((os.path.basename(native_pdb)).replace(".pdb", ""), W, del_nf_G/1000.)
            else:
                print "%s\t\t%3.3f\t\t%3.3f" % ((os.path.basename(native_pdb)).replace(".pdb", ""), W, del_nf_G/1000.)
    else:
        for W in [x * 0.05 for x in range(0, 30)]:                                                                      
            del_nf_G = del_nf_G_apol + del_nf_G_pol  - T * W * (nf_S_bu2ex + nf_S_ex2u + nf_S_bb)
            print "%s\t\t%3.2f\t\t%3.2f" % ((os.path.basename(native_pdb)).replace(".pdb", ""), W, del_nf_G/1000.)
            #f.write("%s\t\t%3.2f\t\t%3.2f\n" % ((os.path.basename(native_pdb)).replace(".pdb", ""), W, del_nf_G/1000.))
        ###f.close()
    #print "initial reference : delH_nf = %f kcal/mol\n" % ((del_nf_G_apol + del_nf_G_pol)/1000.)
    #print "initial reference : T*delS_nf = %f kcal/mol\n" % (T * (nf_S_bu2ex + nf_S_ex2u + nf_S_bb)/1000.)


def fetchsome(cursor, arraysize):
    # a geneator that simplified the use of fetchmany
    while True:
        results = cursor.fetchmany(arraysize)
        if not results: break
        for result in results:
            yield result


def concat_DB_chk(DB_concat_dirs, native_pdb, W, T, win_size):

    import sqlite3 as lite
    import glob
    from collections import defaultdict

    prefix = native_pdb.split("/")[-2]

    FUsum_stat = defaultdict(int)
    #DB_sum = 0
    for p in range(1, win_size+1):
        # uniq mstate number per partitioning
        uniq_mstate = defaultdict(int)
        for sub_dir in DB_concat_dirs:
            db_name = "%s_partition%d_win%s_W%s.db" % (prefix, p, str(win_size), str(W))
            small_db = os.path.join(sub_dir, db_name)
            ###print small_db
            with lite.connect(small_db) as scon:
                scur = scon.cursor()
                scur.execute("select mstateID, FUsum from MC_DB")
                for row in scur.fetchall():
                    if uniq_mstate.has_key(int(row[0])):
                        uniq_mstate[int(row[0])] += 1
                    else:
                        uniq_mstate[int(row[0])] += 1
                        FUsum_stat[int(row[1])] += 1

        print "chk redundant mstateID..partition%d" % p,
        mykeys = uniq_mstate.keys()
        mykeys.sort()
        redundant_chk = 0
        for k in mykeys:
            if uniq_mstate[k] > 1:
                #print k,
                redundant_chk = 1
        if redundant_chk == 0:
            print "........All mstateIDs are uniq in MC_DB"
            print "........Total uniq mstateIDs = %d" % (len(mykeys))
        else:
            print "........Some mstateIDs are NOT uniq in MC_DB"
            print "........Total sampled mstateIDs = %d" % sum(uniq_mstate.itervalues())
            print "........Total uniq mstateIDs = %d" % len(mykeys)
        
    print
    print "-------------------------------"
    print "FUsum distribution from MC_DB"
    print "-------------------------------"
    print "FUsum, accumulated mstates"
    mykeys = FUsum_stat.keys()
    mykeys.sort()
    total_mstate = 0
    for k in mykeys:
        total_mstate += FUsum_stat[k]
        print k, FUsum_stat[k]
    print "========================="
    print "tot. mstates = %d" % total_mstate
    print "========================="


def Q_calc(DB_concat_dirs, native_pdb, W, T, win_size):

    prefix = native_pdb.split("/")[-2]
    
    from collections import defaultdict
    ###################################################################
    #
    # per partitioning, just get total Q_p
    #
    Q = 0.0e+0                                                                                                 
    Q_p_dic = {}
    for p in range(1, win_size+1):
        Q_p = 0.0e+0                                            
        # uniq mstate number per partitioning
        uniq_mstate = defaultdict(int)
        for sub_dir in DB_concat_dirs:
            db_name = "%s_partition%d_win%s_W%s.db" % (prefix, p, str(win_size), str(W))
            small_db = os.path.join(sub_dir, db_name)
            #print small_db
            with lite.connect(small_db) as scon:                  
                scur = scon.cursor()
                scur.execute("select delG_i from MC_DB")
                for row in scur.fetchall():
                    delG_i = row[0]
                    K_i_p = math.exp(-1.0*delG_i/(constR_kcal*T))
                    Q_p += K_i_p
        Q_p_dic[p] = Q_p
        print "Q_%d = %e" % (p, Q_p)
        Q += Q_p
    print "============================"
    print  "Q_total = %e" % Q
    print "============================"

    return Q, Q_p_dic


def PFj_calc(DB_concat_dirs, native_pdb, W, T, win_size, Q, Q_p_dic, exposure_cutoff, calc_dir, calc_step = 3):

    from collections import defaultdict
    from itertools import product, repeat

    # here need to include intrinsic exposure in the native by using nat_frac_asa_dic info
    # to update P_exp_f
    native_info_dic, nat_frac_asa_dic = get_native_info_dic(native_pdb, vadar_parm_dir)

    # partition information
    partition_string_list = COREX_partition(win_size, tot_seq, min_unit = 4, print_option = 0)
    

    partitionInfo = defaultdict(list)
    s1 = [(p+1, partition_string) for p, partition_string in enumerate(partition_string_list)]
    s2 = [(p+1, len(partition_string.split())) for p, partition_string in enumerate(partition_string_list)]
    for k, v in s1:
        partitionInfo[k].append(v)
    for k, v in s2:
        partitionInfo[k].append(v)
    
    #partitionSeq = partitionInfo[p][0]  # 7 8 8 8 8 8 4 (each number represents a number of residues in a FU
    #FUs = partitionInfo[p][1]           # total number of FUs

    prefix = native_pdb.split("/")[-2]
    if not os.path.exists(calc_dir):
        os.mkdir(calc_dir)

    ###################################################################
    #
    # DB creation and insert mstateID, P_i, ExpRes
    #
    if calc_step == 1:
        #
        # calculate probability of each microstate                                                                                                                                          
        #
        partitions = partitionInfo.keys()
        for p in partitions:
            p = int(p)
            # DB creation per partition
            db_name_ = "%s_partition%d_win%s_W%s.db" % (prefix, p, str(win_size), str(W))
            DB_p = os.path.join(calc_dir, db_name_) 


            # ipark: CRITICAL!!! to prevent appending data to existing DB
            format = "%a-%b-%d-%H:%M:%S-%Y"                        
            today = datetime.datetime.today()
            s = today.strftime(format)
            if os.path.exists(DB_p):
                os.rename(DB_p, DB_p + '.' + s)
                print "renaming existing MC_DB_concat................%d" % p
            else:
                print "creating new MC_DB_concat................%d" % p

            with lite.connect(DB_p) as con:                       
                cur = con.cursor()
                # ipark: CRITICAL!!! this append new data not update 
                #con.execute("CREATE TABLE IF NOT EXISTS MC_DB_concat (mstateID INTEGER PRIMARY KEY, P_i REAL, ExpRes TEXT);")
                con.execute("CREATE TABLE MC_DB_concat (mstateID INTEGER PRIMARY KEY, P_i REAL, ExpRes TEXT);")

            # Unique mstateID chk per partition
            uniq_mstate = defaultdict(int)
            ###############################

            for sub_dir in DB_concat_dirs:
                db_name = "%s_partition%d_win%s_W%s.db" % (prefix, p, str(win_size), str(W))
                small_db = os.path.join(sub_dir, db_name)
                print "\t\t%s" % '/'.join(sub_dir.split("/")[3:])
                with lite.connect(small_db) as scon:                   
                    scur = scon.cursor()
                    scur.execute("select delG_i, mstateID, ExpRes from MC_DB")
                    for row in scur.fetchall():
                        #### query from MC_DB
                        delG_i = row[0]
                        mstateID = row[1]; uniq_mstate[mstateID] += 1 # in order to trace mstateID's uniqueness
                        ExpRes = row[2]
                        #### insert into MC_DB_concat
                        #### before insert, check mstateID's uniqueness
                        if uniq_mstate[mstateID] == 1:
                            K_i_p = math.exp(-1.0*delG_i/(constR_kcal*T)) 
                            P_i = K_i_p/Q
                            info_tuple = (mstateID, P_i, ExpRes)
                            ###print info_tuple                                                 
                            cur.execute("INSERT INTO MC_DB_concat VALUES(?, ?, ?)", info_tuple)
                            con.commit()


    ###################################################################
    #
    # residue-wise Fold and Unfold probability to PF_J_TABLE DB
    #
    elif calc_step == 2:
        #

        arraysize = 500

        # over all partitions|||||||||||||||||||||||||||||||||
        info_fetch_all = []

        PF_fold_info = {}
        PF_unfold_info = {}

        P_exp_f = {}

        PF_db = os.path.join(calc_dir, "%s_PFj_win%s_W%s.db" % (prefix, str(win_size), str(W)))             
        if os.path.exists(PF_db):
            format = "%a-%b-%d-%H:%M:%S-%Y"                        
            today = datetime.datetime.today()
            s = today.strftime(format)
            os.rename(PF_db, PF_db + '.' + s)
            print "renaming existing %s" % PF_db 
        else:   
            print "creading new %s" % PF_db 

        conpf = lite.connect(PF_db)
        with conpf:                                                                                         
            curpf = conpf.cursor()
            # ipark: CRITICAL!!! this append new data not update 
            #curpf.execute("CREATE TABLE IF NOT EXISTS PF_J_TABLE(resID INTEGER, FoldSum_Pij REAL, UnfoldSum_Pij REAL, P_exp_f REAL);")
            curpf.execute("CREATE TABLE PF_J_TABLE(resID INTEGER, FoldSum_Pij REAL, UnfoldSum_Pij REAL, P_exp_f REAL);")

        #partitions----------------------------------------------------
        partitions = partitionInfo.keys()
        for p in partitions:

            p = int(p)
            partitionSeq = partitionInfo[p][0]  # 7 8 8 8 8 8 4 (each number represents a number of residues in a FU 
            FUs = partitionInfo[p][1]           # total number of FUs


            # DB creation per partition
            db_name_ = "%s_partition%d_win%s_W%s.db" % (prefix, p, str(win_size), str(W))
            print "\t\t%s" % (db_name_)
            DB_p = os.path.join(calc_dir, db_name_) 
            con = lite.connect(DB_p)

            with con:
                cur = con.cursor()
                """
                without ExpRes
                cur.execute("SELECT mstateID, P_i from MC_DB_concat;")
                """
                # ipark: manipulation by excluding too high prob.
                cur.execute("SELECT mstateID, P_i, ExpRes from MC_DB_concat;")
                #cur.execute("SELECT mstateID, P_i, ExpRes from MC_DB_concat where P_i < 1.0e-4;")


                #microstates////////////////////////////////
                for row in fetchsome(cur, arraysize):
                    #
                    # convert mstateID --> binary number
                    #
                    i = row[0]
                    # ipark: manupulation 
                    #prob_i = row[1]
                    prob_i = row[1] * (Q/Q_p_dic[p])   # outperformed!
                    ExpResList = row[2]

                    int2bin = eval("'{0:0%db}'.format(i)" % FUs) 
                    ###FUsum = sum([int(f)  for f in int2bin])
    
                    #
                    # then, construct complete binary fold seqeunce given binary_num * partitionSeq
                    # here generate a full fold-flag string by product of int2bin(1/0 comb) x partitionSeq (7 8 8 ...4)
                    #
                    fold_flag = []
                    for s, bit in enumerate(int2bin):
                        if int(bit) == 1: # fold
                            fold_flag.extend([z for z in repeat(1, int(partitionSeq.split()[s]))])
                        else: # unfold
                            fold_flag.extend([z for z in repeat(0, int(partitionSeq.split()[s]))])

                    # with a full fold_flag WITHOUT ExpRes
                    for j, jval in enumerate(list(fold_flag)):
                        if jval == 1:
                            if PF_fold_info.has_key(j+1):
                                old_partial_fold_sum = PF_fold_info[j+1]
                                newFold = {j+1:old_partial_fold_sum + prob_i}
                                PF_fold_info.update(newFold)
                            else:
                                PF_fold_info[j+1] = prob_i
                        elif jval == 0:
                            if PF_unfold_info.has_key(j+1):
                                old_partial_unfold_sum = PF_unfold_info[j+1]
                                newUnfold = {j+1:old_partial_unfold_sum + prob_i}
                                PF_unfold_info.update(newUnfold)
                            else:
                                PF_unfold_info[j+1] = prob_i
                    
                    if ExpResList == 'NONE':
                        continue 
                    else:
                        # with a full fold_flag WITH ExpRes ==> jval == 1 (fold) but belongs to ExpRes 
                        for j, jval in enumerate(list(fold_flag)):
                            if jval == 1:
                                """
                                for expres_j in ExpResList.split(','):                        
                                    expres_j = int(expres_j)
                                    #print type(expres_j)
                                    if j+1 == expres_j:
                                """
                                if any(j+1 == int(expres_j) for expres_j in ExpResList.split(',')):
                                    if P_exp_f.has_key(j+1):                             
                                        old_p_exp_f_sum = P_exp_f[j+1]
                                        new_p_exp_f = {j+1:old_p_exp_f_sum + prob_i}
                                        P_exp_f.update(new_p_exp_f)
                                    else:
                                        P_exp_f[j+1] = prob_i
                                if nat_frac_asa_dic[j+1][1] > exposure_cutoff:
                                    if P_exp_f.has_key(j+1):                             
                                        old_p_exp_f_sum = P_exp_f[j+1]
                                        new_p_exp_f = {j+1:old_p_exp_f_sum + prob_i}
                                        P_exp_f.update(new_p_exp_f)
                                    else:
                                        P_exp_f[j+1] = prob_i
                """
                if PF_unfold_info.has_key(100):
                    print p, PF_unfold_info[100]
                else:
                    print p 
                """
                #microstates////////////////////////////////
        #partitions----------------------------------------------------
        foldKeys = PF_fold_info.keys(); foldKeys.sort()                               
        unfoldKeys = PF_unfold_info.keys(); unfoldKeys.sort()
        fetch_keys = set(foldKeys + unfoldKeys)
        for mID in fetch_keys:
            if not PF_fold_info.has_key(mID):
                PF_fold_info[mID] = -1000
            elif not PF_unfold_info.has_key(mID):
                PF_unfold_info[mID] = -1000
            elif not P_exp_f.has_key(mID):
                P_exp_f[mID] = -1000
            info_fetch_all.append((mID,)+(PF_fold_info[mID],)+(PF_unfold_info[mID],)+(P_exp_f[mID],))
        ###print info_fetch_all
        #
        with conpf:                                                                                          
            query = "INSERT INTO PF_J_TABLE VALUES(?, ?, ?, ?);"
            curpf.executemany(query, info_fetch_all)
        # over all partitions|||||||||||||||||||||||||||||||||
    ###################################################################
    #
    # residue-wise PFj from PF_J_TABLE DB
    #
    elif calc_step == 3:
        #
        PFj_dic = defaultdict(list) 
        #
        format = "%a-%b-%d-%H:%M:%S-%Y"                       
        today = datetime.datetime.today()
        s = today.strftime(format)
        pf_out = "PF_j.out.%s" % (s)
        f = open(os.path.join(calc_dir, pf_out), 'w')
        f.write("residue\tPFj\tlnPFj\tPFj_exp_f\tlnPFj_exp_f\n")
    
        
        for j in native_info_dic['res_num']:
            #fold_sum_j = []
            #unfold_sum_j = []
            #p_exp_f_sum_j = []
            fold_sum_j = 0.0
            unfold_sum_j = 0.0
            p_exp_f_sum_j = 0.0

            if native_info_dic['res_name'][j-1] == 'PRO':
                #print j, native_info_dic['res_name'][j-1]
                f.write("%d\t%f\t%f\t%f\t%f\n" % (j, 0.0, 0.0, 0.0, 0.0))
                PFj_dic[j].append(0.0)
                PFj_dic[j].append(0.0)
            else: # non-Pro residues
                PF_db = os.path.join(calc_dir, "%s_PFj_win%s_W%s.db" % (prefix, str(win_size), str(W)))                             
                with lite.connect(PF_db) as pfcon:
                    pfcur = pfcon.cursor()
                    #                                                                                              
                    # fold_sum
                    pfcur.execute("SELECT FoldSum_Pij from PF_J_TABLE WHERE resID = %d AND NOT FoldSum_Pij=-1000;" % j)
                    #print pfcur.fetchone()
                    #fold_sum_j.append(pfcur.fetchone()[0])
                    fold_sum_j = pfcur.fetchone()[0]
                    """
                    if pfcur.fetchone() is None:
                        fold_sum_j.append(0.0)
                    else:
                        pfcur.execute("SELECT sum(FoldSum_Pij) from PF_J_TABLE WHERE resID = %d AND NOT FoldSum_Pij=-1000;" % j)
                        fold_sum_j.append(pfcur.fetchone()[0])
                    """
                    # 
                    # unfold_sum
                    pfcur.execute("SELECT UnfoldSum_Pij from PF_J_TABLE WHERE resID = %d AND NOT UnfoldSum_Pij=-1000;" % j)
                    #print pfcur.fetchone()
                    #unfold_sum_j.append(pfcur.fetchone()[0])
                    unfold_sum_j = pfcur.fetchone()[0]
                    """
                    if pfcur.fetchone() is None:
                        unfold_sum_j.append(0.0)
                    else:
                        pfcur.execute("SELECT sum(UnfoldSum_Pij) from PF_J_TABLE WHERE resID = %d AND NOT UnfoldSum_Pij=-1000;" % j)
                        unfold_sum_j.append(pfcur.fetchone()[0])
                    """
                    # 
                    # p_exp_f_sum
                    pfcur.execute("SELECT P_exp_f from PF_J_TABLE WHERE resID = %d AND NOT P_exp_f=-1000;" % j)
                    if pfcur.fetchone() is None:
                        #p_exp_f_sum_j.append(0.0)
                        p_exp_f_sum_j = 0.0
                    else:
                        #pfcur.execute("SELECT sum(P_exp_f) from PF_J_TABLE WHERE resID = %d AND NOT P_exp_f=-1000;" % j)
                        pfcur.execute("SELECT P_exp_f from PF_J_TABLE WHERE resID = %d AND NOT P_exp_f=-1000;" % j)
                        #p_exp_f_sum_j.append(pfcur.fetchone()[0])
                        p_exp_f_sum_j = pfcur.fetchone()[0]
                                                                                                                                     
                #print 'res:{:02d}, S_fold_j:{:.5e}, S_unfold_j:{:.5e}, S_p_exp_f_j:{:.5e}'.format(j, sum(fold_sum_j), sum(unfold_sum_j), sum(p_exp_f_sum_j))
                #PF_j = sum(fold_sum_j)/sum(unfold_sum_j)                                                      
                PF_j = fold_sum_j/unfold_sum_j
                """
                best correlation 
                unfold_sum_j = unfold_sum_j * 1.0e+12
                PF_j_exp_f = (fold_sum_j - p_exp_f_sum_j)/(unfold_sum_j + p_exp_f_sum_j)
                """
                PF_j_exp_f = (fold_sum_j - p_exp_f_sum_j)/(unfold_sum_j + p_exp_f_sum_j)
                                                                                                                                     
                PFj_dic[j].append(PF_j)
                PFj_dic[j].append(PF_j_exp_f)

                if PF_j <= 0.0:
                    log_PF_j = 0.0 
                    log_PF_j_exp_f = math.log(PF_j_exp_f)
                elif PF_j_exp_f <= 0.0:
                    log_PF_j = math.log(PF_j)             
                    log_PF_j_exp_f = 0.0 
                else:
                    log_PF_j = math.log(PF_j)             
                    log_PF_j_exp_f = math.log(PF_j_exp_f)

                f.write("%d\t%e\t%e\t%e\t%e\n" % (j, PF_j, log_PF_j, PF_j_exp_f, log_PF_j_exp_f))
        f.close()
        """                          
        residues = PFj_dic.keys()
        residues.sort()
        for r in residues:
            print r, PFj_dic[r]
        """
        #return PFj_dic
        return pf_out

............................ OMIT .........................

###################################################################################################

def main_delG_unfold_chk(native_pdb, T, user_input_W):
    # 1st : check delG of unfolded state, and determine W for it making positive
    """
    for native_pdb in glob.glob("Reparameterization/Murpy_Freire_set/????.pdb"):
        native_info_dic = get_native_info_dic(native_pdb)  
        delG_unfold_chk(native_info_dic, native_pdb, T, user_input_W = 0)
    """
    delG_unfold_chk(native_pdb, T, user_input_W)

def main_partitioning(win_size, tot_seq):
    # partitioning & combinatorial enumeration of microstate (with ASA calculation)
    partition_string_list = COREX_partition(win_size, tot_seq, min_unit = 4, print_option = 0)
    return partition_string_list
    #COREX_combinatorial_enumeration_per_FUs(partition_string_list, win_size, min_unit, tot_seq)


def main_MC(native_pdb, W, T, exposure_cutoff, sub_ensemble, win_size, partition_calc, ASAcalc_dir):
    # select low-energy microstate via MC
    partition_string_list = main_partitioning(win_size, tot_seq)
    COREX_microstate_ensemble_selection_via_MC_OnTheFly(partition_string_list, native_pdb, W, T, exposure_cutoff, sub_ensemble, win_size, partition_calc, ASAcalc_dir)


def main_PFj_calc(MC_DB_root, native_pdb, W, T, win_size, exposure_cutoff, calc_dir, ASAcalc_dir):

    # put your sub-ensemble DB paths
    DB_concat_dirs =  [os.path.join(MC_DB_root, sub_dir) for sub_dir in 
(ASAcalc_dir ,)] # <--- here "comma" is crutial especially for a single element case
    print DB_concat_dirs
    # check redundancy of sampled microstate per partition,
    # if no redundancy found, concat all DBs into one per partition.
    # otherwise, concat them by uniq mstateID
    # RUN:
    concat_DB_chk(DB_concat_dirs, native_pdb, W, T, win_size)
    # OUTPUT:
    """
    -------------------------------     -------------------------------
    FUsum distribution from MC_DB       FUsum distribution from MC_DB
    -------------------------------     -------------------------------
    FUsum, accumulated mstates          FUsum, accumulated mstates
    4 9                                 4 11
    5 32                                5 46
    6 105                               6 130
    7 507                               7 610
    8 1760                              8 2079
    9 4842                              9 5625
    10 9335                             10 11000
    11 12734                            11 15124
    12 10950                            12 13158
    13 5292                             13 6697
    14 1489                             14 1961
    15 241                              15 346
    16 28                               16 36
    17 2                                17 2
    =========================           =========================
    tot. mstates = 47326                tot. mstates = 56825
    =========================           =========================
    """


    # partition function
    # total partition function : Q
    # partition-specific partition function : Q_p
    Q, Q_p_dic = Q_calc(DB_concat_dirs, native_pdb, W, T, win_size)

    PFj_calc(DB_concat_dirs, native_pdb, W, T, win_size, Q, Q_p_dic, exposure_cutoff, calc_dir, calc_step = 1)
    PFj_calc(DB_concat_dirs, native_pdb, W, T, win_size, Q, Q_p_dic, exposure_cutoff, calc_dir, calc_step = 2)
    pf_out = PFj_calc(DB_concat_dirs, native_pdb, W, T, win_size, Q, Q_p_dic, exposure_cutoff, calc_dir, calc_step = 3)

def intrinsic_exposure(native_pdb, exposure_cutoff):
    native_info_dic, nat_frac_asa_dic = get_native_info_dic(native_pdb)  
    for j in native_info_dic['res_num']:
        if nat_frac_asa_dic[j][1] > exposure_cutoff:
            print j, nat_frac_asa_dic[j][1]

def MC_run_thread(partition_calc, sub_ensemble):
    main_MC(native_pdb, W, T, exposure_cutoff, sub_ensemble, win_size, partition_calc, ASAcalc_dir)
    return
    

if __name__ == '__main__':
    #
    # RUN inside Script_py dir

    T = 25.+273.                                                                      # temperature in K
    native_pdb = "/home/ipark/DXCOREX_Ubuntu/FAS-TE/FAS-TE-mon.pdb"                   # structure 
    win_size = 12; tot_seq = 282                                                      # user-defined

    vadar_parm_dir = "/home/ipark/DXCOREX_Ubuntu/ASA_VADAR_RichardsRadii_EisenbergPolarApolar"   # ASA calculation 

    # 0st : figure out partitioning and number of microstate
    # RUN:
    COREX_partition(win_size, tot_seq, min_unit = 4, print_option = 1)

    # 1st : check delG of unfolded state, and determine W for it making positive
    # RUN:
    main_delG_unfold_chk(native_pdb, T, user_input_W = 0.9)

    # 2nd: MC selection
    # ps -F --ppid=13118 
    # RUN:
    p_list = main_partitioning(win_size, tot_seq)
    exposure_cutoff = 0.6       # for P_ex,f,j calculation
    # whole
    W = 0.9                    # pre-calcaulted delG = 52 kcal/mol
    ASAcalc_dir = "v14_FAS-TE-mon_W0.90"
    
    """
    sub_ensemble_size_p = {}
    for k, v in enumerate(p_list):
        sub_ensemble_size_p[k+1] = 2**len(v.split())
    print "partition#: max_sub_ensemble_size"
    print sub_ensemble_size_p 

    import multiprocessing
    for partition_calc in sub_ensemble_size_p.keys(): 
        full_enumeration = sub_ensemble_size_p[partition_calc]
        sub_ensemble = int(math.ceil(sub_ensemble_size_p[partition_calc])*1.0e-1)  # 1.0e-1 is used to reduce sub_ensemble size, user defined
        print 'partition {:2d} | sampling_size = {:,} | full_enumeration = {:,}'.format(partition_calc, sub_ensemble, full_enumeration)
        print "================================================================================="
        t = multiprocessing.Process(target = MC_run_thread, args=(partition_calc, sub_ensemble,))
        t.start()
        time.sleep(2)
    """

    # 3rd: MC_DB chk and PFj calc.
    #intrinsic_exposure(native_pdb, exposure_cutoff)
    MC_DB_root=native_pdb.replace("/"+native_pdb.split('/')[-1], "")
    calc_dir = os.path.join(MC_DB_root, "PFj_calc")
    main_PFj_calc(MC_DB_root, native_pdb, W, T, win_size, exposure_cutoff, calc_dir, ASAcalc_dir)

#/end{minted}
