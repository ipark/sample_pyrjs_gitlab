Simple and Clean Interface for Viewing TV Shows
using javascript/css/jQuery/ajax etc.

1. Open the iTV11.html file with Chrome or Firefox.

2. [Search]
Type key word ('+' instead of space) and hit ENTER, then list out TV shows.

3. [Add boomark]
You can save that key word search for later re-use by clicking 'red pin' icon.

4. [Delete bookmark]
You can also DELETE the bookmark by clicking 'x' of the button. 

Unless you delete all browsing history, your up-to-date bookmarks are preserved after closing/opening your browser.